using Foundation;
using System;
using UIKit;

namespace App4
{
    public partial class AllAnswersViewController : UITableViewController
    {
        public TestData test;


        public AllAnswersViewController(IntPtr handle) : base(handle)
        {

        }
        public override void ViewDidLoad()
        {
            try
            {
                AllAnswersTable.Source = new AllAnswersTableViewSource(test);
                AllAnswersTable.RowHeight = UITableView.AutomaticDimension;
                TableView.EstimatedRowHeight = 40f;
                TableView.ReloadData();
                base.ViewDidLoad();
            }
            catch 
            {
                var alert = UIAlertController.Create(Localizable.Str("������!"), null, UIAlertControllerStyle.Alert);
                var atr = new NSAttributedString(Localizable.Str("������ ��� ������") +$" {test.FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                alert.SetValueForKey(atr, new NSString("attributedTitle"));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("�� :("), UIAlertActionStyle.Cancel, (UIAlertAction obj) => NavigationController.PopToRootViewController(true)));
                this.PresentViewController(alert, true, null);
            }
        }
    }
}