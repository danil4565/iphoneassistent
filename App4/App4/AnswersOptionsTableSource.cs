﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SaturdayMP.XPlugins.iOS;


namespace App4
{
    class AnswersOptionsTableSource : UITableViewSource
    {
        AllCellsData cellsData;
        BEMCheckBoxGroup radioGroup;
        //List<Questions.Answers> answersList;
        Questions Questions;
        int collectionCellId;
        bool MultipleAnswers = false;

        public AnswersOptionsTableSource(AllCellsData cellsData, Questions Questions, int cellId, bool MultAnswers)
        {
            MultipleAnswers = MultAnswers;
            collectionCellId = cellId;
            //answersList = Questions.answers;
            this.Questions = Questions;
            radioGroup = new BEMCheckBoxGroup();

            this.cellsData = cellsData;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (!Questions.MultipleAnswer)
            {
                var cell = (AnswerOptionCell)tableView.DequeueReusableCell(AnswerOptionCell.reuseIndentifier, indexPath);

                cell.UpdateCell(Questions.answers[indexPath.Row].Answer, cellsData, radioGroup, collectionCellId, indexPath.Row);

                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                return cell;
            }
            else
            {
                var cell = (MultipleAnswerOptionCell)tableView.DequeueReusableCell(MultipleAnswerOptionCell.reuseIndentifier, indexPath);

                cell.UpdateCell(Questions.answers[indexPath.Row].Answer, cellsData, collectionCellId, indexPath.Row);
                
                cell.SelectionStyle = UITableViewCellSelectionStyle.None;
                return cell;
            }
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return Questions.answers.Count;
        }

        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return UITableView.AutomaticDimension;
        }

    }
}