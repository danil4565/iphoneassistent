﻿using Foundation;
using System;
using UIKit;

namespace App4
{
    internal class ConstructorAnswersCollectionViewSource : UICollectionViewSource
    {

        AllCellsData cellsData;
        UICollectionView questionsCollection;

        public ConstructorAnswersCollectionViewSource(UICollectionView questionListCollection, AllCellsData allCellsData)
        {
            questionsCollection = questionListCollection;
            cellsData = allCellsData;

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            if (indexPath.Row == cellsData.questionCells.Count)
            {
                var cell = (ConstructorAddAnswersCollectionCell)collectionView.DequeueReusableCell(ConstructorAddAnswersCollectionCell.reuseIdentifier, indexPath);
                cell.UpdateCell(collectionView, indexPath, cellsData, questionsCollection);
                return cell;
            }
            else
            {
                var cell = (ConstructorAnswersCollectionCell)collectionView.DequeueReusableCell(ConstructorAnswersCollectionCell.reuseIdentifier, indexPath);
                cell.UpdateCell(cellsData, indexPath.Row);
                return cell;
            }

        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return cellsData.questionCells.Count + 1;
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }

        public override void WillDisplayCell(UICollectionView collectionView, UICollectionViewCell cell, NSIndexPath indexPath)
        {
            if (indexPath.Row != cellsData.questionCells.Count)
            {
                var cell2 = (ConstructorAnswersCollectionCell)cell;
                cell2.answersTable.ReloadData();
                collectionView.ReloadItems(new NSIndexPath[] { indexPath });
            }
        }

    }
}