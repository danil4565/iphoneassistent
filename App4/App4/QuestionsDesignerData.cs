﻿using ICSharpCode.SharpZipLib.Zip;
using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using UIKit;
using Xamarin.Essentials;

namespace App4
{
    public class QuestionsDesignerData
    {
        private AllCellsData AllCellsData;
        public string Cache;
        public string FilePathSaveTest;
        public bool Check = false;

        public QuestionsDesignerData(AllCellsData AllCellsData)
        {
            this.AllCellsData = AllCellsData;
            Cache = Path.Combine(FileSystem.CacheDirectory, "Share");
            Directory.CreateDirectory(Cache);
            Check = CheckTest();
        }

        public  bool CheckTest()
        {

            try
            {
                bool CheckTryAnswer = false;
                foreach (var Q in AllCellsData.questionCells)
                {

                    if (Q.Question == null || Q.Question == "")
                    {
                        Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("Не задан текст вопроса"));
                        return false;
                    }
                    foreach (var A in Q.answers)
                    {
                        if (A.Text == null || A.Text == "")
                        {
                            Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("Не задан текст варианта ответа"));
                            return false;
                        }
                        if (A.IsCorrect)
                        {
                            CheckTryAnswer = true;
                        }
                    }
                    if (!CheckTryAnswer)
                    {
                        Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2.2, 0.4, Localizable.Str("Не во всех вопросах указан правильный вариант ответа"));
                        return false;
                    }
                }

                return true;
            }
            catch
            {
                Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("В тесте есть ошибка"));
                return false;
            }
        }



        private List<string> CreatingTest()
        {
            var Test = new List<string>();

            try
            {

                foreach (var Q in AllCellsData.questionCells)
                {
                    Test.Add("?");
                    Test.Add(Q.Question);
                    foreach (var A in Q.answers)
                    {
                        if (!A.IsCorrect)
                        {
                            Test.Add($"-{A.Text}");
                        }
                        else
                        {
                            Test.Add($"+{A.Text}");
                        }
                    }
                }
                return Test;
            }
            catch { return Test; }

        }
        public void SaveTestQst(string FileName)
        {
            var Test = CreatingTest();
            string FilePathSaveTest = Path.Combine(Cache, FileName + ".qst");
            try
            {
                if (Check)
                {
                    File.WriteAllLines(FilePathSaveTest, Test, Encoding.GetEncoding(1251));
                    this.FilePathSaveTest = FilePathSaveTest;
                }
                else { this.FilePathSaveTest = null; }
            }
            catch { this.FilePathSaveTest = null; }

        }

        public void SaveTestTxt(string FileName)
        {
            var Test = CreatingTest();
            string FilePathSaveTest = Path.Combine(Cache, FileName + ".txt");

            try
            {
                if (Check)
                {
                    File.WriteAllLines(FilePathSaveTest, Test, Encoding.GetEncoding(1251));
                    this.FilePathSaveTest = FilePathSaveTest;
                }
                else { this.FilePathSaveTest = null; }
            }
            catch { this.FilePathSaveTest = null; }
        }
        public void SaveTestIt2(string FileName)
        {

            string FilePathSaveTest = Path.Combine(Cache, FileName + ".it2");
            string PathBuferZip = Path.Combine(Cache, "DesingerIt2");
            Directory.CreateDirectory(PathBuferZip);
            FastZip zip = new FastZip();
            try
            {
                if (Check)
                {
                    XDocument XmlDoc = new XDocument(new XDeclaration("1.0", "uft-8", null),
                                                     new XElement("irenTest", new XAttribute("version", "2"), new XAttribute("xmlns_xsi", "http://www.w3.org/2001/XMLSchema-instance"),
                                                                                                              new XAttribute("xsi_noNamespaceSchemaLocation", "http://irenproject.ru/schema/it2.xsd"),
                                                        new XElement("section", new XAttribute("title", "Тест"),
                                                            new XElement("questions",
                                                            Enumerable.Range(0, AllCellsData.questionCells.Count).Select(question =>
                                                                 new XElement("question", new XAttribute("type", "select"), new XAttribute("id", $"{question + 1}"),
                                                                                                                            new XAttribute("weight", "1"),
                                                                                                                            new XAttribute("enabled", "true"),
                                                                   new XElement("content",
                                                                      new XElement("text", new XAttribute("value", AllCellsData.questionCells[question].Question))),
                                                                   new XElement("choices",
                                                                      Enumerable.Range(0, AllCellsData.questionCells[question].answers.Count).Select(choice =>
                                                                      new XElement("choice", new XAttribute("correct", AllCellsData.questionCells[question].answers[choice].IsCorrect.ToString().ToLower()), new XAttribute("fixed", "false"), new XAttribute("negative", "false"),
                                                                          new XElement("content",
                                                                              new XElement("text", new XAttribute("value", AllCellsData.questionCells[question].answers[choice].Text))))))))))));

                    XmlDoc.Save(Path.Combine(PathBuferZip, "test.xml"));
                    zip.CreateZip(FilePathSaveTest, PathBuferZip, true, "test.xml");
                    Directory.Delete(PathBuferZip, true);
                    this.FilePathSaveTest = FilePathSaveTest;
                }
                else { this.FilePathSaveTest = null; }
            }
            catch { this.FilePathSaveTest = null; }

        }
        public void SaveTestQsz(string FileName)
        {
            try
            {
                if (Check)
                {
                    string FilePathSaveTest = Path.Combine(Cache, FileName + ".qsz");
                    var ResultArr = new List<byte>();
                    var EncodingFiles = Encoding.GetEncoding(1251);
                    foreach (var Q in AllCellsData.questionCells)
                    {
                        ResultArr.AddRange(BitConverter.GetBytes(Q.Question.Length));
                        ResultArr.AddRange(EncodingFiles.GetBytes(Q.Question));
                        ResultArr.AddRange(BitConverter.GetBytes(Q.answers.Count));
                        foreach (var A in Q.answers)
                        {
                            ResultArr.AddRange(BitConverter.GetBytes(A.Text.Length));
                            ResultArr.AddRange(EncodingFiles.GetBytes(A.Text));
                            if (A.IsCorrect)
                            {
                                ResultArr.AddRange(BitConverter.GetBytes(-1));
                            }
                            else
                            {
                                ResultArr.AddRange(BitConverter.GetBytes(0));
                            }
                        }
                    }

                    File.WriteAllBytes(FilePathSaveTest, ZlibStream.CompressBuffer(ResultArr.ToArray()));
                    this.FilePathSaveTest = FilePathSaveTest;
                }
                else { this.FilePathSaveTest = null; }
            }
            catch
            {
                Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("Проверьте правильность созданного теста!!!"));
                this.FilePathSaveTest = null;
            }

            // -1 правильно
            // 0 неправильно
        }
    }
}