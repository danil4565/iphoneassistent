using Foundation;
using System;
using UIKit;
using Syncfusion.iOS.Buttons;

namespace App4
{
    public partial class SingleAnswerCell : UITableViewCell
    {
        public SingleAnswerCell (IntPtr handle) : base (handle)
        {
            HeightAnchor.ConstraintEqualTo(Bounds.Height);
            WidthAnchor.ConstraintEqualTo(Bounds.Width);
        }
        public void UpdateCell(string answer, SfRadioGroup radioGroup)
        {
           
        }
    }
}