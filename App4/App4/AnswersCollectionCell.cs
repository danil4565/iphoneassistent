//#define RELEASE
using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace App4
{
    public partial class AnswersCollectionCell : UICollectionViewCell
    {
        public static string reuseIdentifier { get; } = "answersCollectionCell";


        public UITableView answersTable;
        UILabel question;
        public AnswersCollectionCell (IntPtr handle) : base (handle)
        {
            question = new UILabel();

            question.Lines = 0;

            AddSubview(question);

            question.TranslatesAutoresizingMaskIntoConstraints = false;

            question.TopAnchor.ConstraintEqualTo(TopAnchor, 5).Active = true;
            question.RightAnchor.ConstraintEqualTo(RightAnchor, -15).Active = true;
            question.LeftAnchor.ConstraintEqualTo(LeftAnchor, 15).Active = true;

            question.Font = UIFont.SystemFontOfSize(18);
            

            answersTable = new UITableView();
            answersTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            answersTable.RowHeight = UITableView.AutomaticDimension;
            answersTable.EstimatedRowHeight = 40;
            answersTable.RegisterClassForCellReuse(typeof(AnswerOptionCell), AnswerOptionCell.reuseIndentifier);
            answersTable.RegisterClassForCellReuse(typeof(MultipleAnswerOptionCell), MultipleAnswerOptionCell.reuseIndentifier);
            AddSubview(answersTable);

            answersTable.TranslatesAutoresizingMaskIntoConstraints = false;

            answersTable.TopAnchor.ConstraintEqualTo(question.BottomAnchor, 5).Active = true;
            answersTable.LeftAnchor.ConstraintEqualTo(LeftAnchor).Active = true;
            answersTable.RightAnchor.ConstraintEqualTo(RightAnchor).Active = true;
            answersTable.BottomAnchor.ConstraintEqualTo(BottomAnchor).Active = true;
#if RELEASE
            answersTable.AlwaysBounceVertical = false;
#endif   
            //answersTable.AllowsSelection = false;
            //answersTable.AllowsMultipleSelection = false;
        }
        public void UpdateCell(AllCellsData cellsData, Questions Questions, int cellId, bool MultAnswers)
        {
            answersTable.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;

            question.Text = Questions.Question;

            //answersTable.UserInteractionEnabled = cellsData[cellId].UserInteraction;
            answersTable.Source = new AnswersOptionsTableSource(cellsData, Questions, cellId, MultAnswers);

        }
        public override void AwakeFromNib()
        {
            answersTable.ReloadData();
        }
        
    }
}