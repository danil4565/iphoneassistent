﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    public static class Localizable
    {
        public static string Str(string key)
        {
            return NSBundle.MainBundle.GetLocalizedString(key, null);
        }
    }
}