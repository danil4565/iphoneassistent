using CoreGraphics;
using Foundation;
using System;
using UIKit;

namespace App4
{
    public partial class QuastionsListCell : UICollectionViewCell
    {
        UILabel number;
        public static string reuseIdentifier { get; } = "quastionsListCell";


        protected internal QuastionsListCell(IntPtr handle) : base(handle)
        {
            Layer.BorderColor = IsDark.Dark ? UIColor.LightTextColor.CGColor : UIColor.DarkTextColor.CGColor;
            Layer.BorderWidth = (nfloat)0.5;
            Layer.CornerRadius = this.Frame.Width / 2;
            Layer.MasksToBounds = true;
            number = new UILabel(new CGRect(0, 0, Bounds.Width, Bounds.Height));
            number.BackgroundColor = UIColor.LightTextColor;
            number.TextAlignment = UITextAlignment.Center;
            AddSubview(number);
        }

        public void UpdateCell(int numberOfQuastion, UIColor color)
        {
            number.Text = numberOfQuastion.ToString();
            BackgroundColor = color ?? UIColor.GroupTableViewBackgroundColor;
        }
    }
}
