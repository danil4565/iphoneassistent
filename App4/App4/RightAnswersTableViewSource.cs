﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    class RightAnswersTableViewSource : UITableViewSource
    {
        TestData testData;
        List<List<string>> quastions;

        public RightAnswersTableViewSource(TestData test)
        {
            testData = test;
            quastions = testData.GetRightAnswers();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (RightAnswerTableCell)tableView.DequeueReusableCell("rightAnswerTableCell", indexPath);
            try
            {
                cell.UpdateCell(quastions[indexPath.Row]);
            }
            catch { }
            
            return cell;

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return testData.NumberOfQuastions;
        }

        
    }
}