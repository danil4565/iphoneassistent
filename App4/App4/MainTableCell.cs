using Foundation;
using System;
using UIKit;
using ObjCRuntime;

namespace App4
{
    public partial class MainTableCell : UITableViewCell
    {

        public MainTableCell(IntPtr intPtr) : base(intPtr) { }

        public MainTableCell(NSString nSString): base (UITableViewCellStyle.Default, nSString)
        {
        }

        public void UpdateCell(TestData test)
        {

            TestName.Text = test.RightFileName;
            CreationDate.Text = test.CreationDate;
            NumberOfQuestions.Text = Localizable.Str("���������� ��������: ") + $"{test.NumberOfQuastions}";
            var answersStr = test.IsMultipleAnswersAvailable ? Localizable.Str("��") : Localizable.Str("���");
            MultipleAnswers.Text = Localizable.Str("��������� �������: ") + $"{answersStr}";
        }

        public override void LayoutSubviews()
        {
            
            base.LayoutSubviews();
            
        }
    }
}
