﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    public class AllCellsData
    {
        public List<QuestionCellData> questionCells;
        public QuestionCellData this[int indexer] { get => questionCells[indexer]; }
        public AllCellsData()
        {
            questionCells = new List<QuestionCellData>(1);
            questionCells.Add(new QuestionCellData(1));
        }
        public AllCellsData(List<Questions> questions)
        {
            List<int> questionsAnswersCount = GetNumberOfAnswersInEveryQuestion(questions);
            questionCells = new List<QuestionCellData>(questions.Count);
            for (int i = 0; i < questions.Count; i++)
            {
                questionCells.Add(new QuestionCellData(questions[i]));
            }
        }
        public List<int> GetNumberOfAnswersInEveryQuestion(List<Questions> questions)
        {

            var resultList = new List<int>(questions.Count);
            for (int i = 0; i < questions.Count; i++)
            {
                resultList.Add(questions[i].answers.Count);
            }
            return resultList;

        }
    }

    public class QuestionCellData
    {
        public string Question { get; set; }
        public AnswerCellData this[int indexer] { get => answers.ElementAt(indexer); } ///
        public bool UserInteraction = true;
        public QuestionCellData(int numOfAnswers)
        {
            answers = new List<AnswerCellData>(numOfAnswers);
            for (int i = 0; i < numOfAnswers; i++)
            {
                answers.Add(new AnswerCellData());
            }
        }

        public QuestionCellData(Questions answersAndQuestion)
        {
            Question = answersAndQuestion.Question;
            answers = new List<AnswerCellData>(answersAndQuestion.answers.Count);
            for (int i = 0; i < answersAndQuestion.answers.Count; i++)
            {
                answers.Add(new AnswerCellData() { Text = answersAndQuestion.answers[i].Answer, IsCorrect = answersAndQuestion.answers[i].IsCorrect });
            }
        }

        public int GetSelectedIndex()
        {
            for (int i = 0; i < answers.Count; i++)
            {
                if (answers[i].IsChecked)
                    return i;
            }
            return -1;
        }
        public List<int> GetSelectedIndexes()
        {
            List<int> result = new List<int>();
            for (int i = 0; i < answers.Count; i++)
            {
                if (answers[i].IsChecked)
                    result.Add(i);
            }
            return result;
        }
        public List<AnswerCellData> answers;
    }
    public class AnswerCellData
    {
        public bool IsCorrect { get; set; } = false;
        public string Text { get; set; } = "";
        public bool IsChecked { get; set; } = false;
        public UIColor backgroundColor { get; set; } = null;
    }

}