// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("TestControll")]
    partial class TestControll
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UINavigationItem MainNavigItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        App4.MainTableVIew MainTable { get; set; }

        [Action ("UIBarButtonItem28213_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void UIBarButtonItem28213_Activated (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (MainNavigItem != null) {
                MainNavigItem.Dispose ();
                MainNavigItem = null;
            }

            if (MainTable != null) {
                MainTable.Dispose ();
                MainTable = null;
            }
        }
    }
}