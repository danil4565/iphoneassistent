﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    class QuastionListCollectionViewSource: UICollectionViewSource
    {
        int numberOfRows;
        public QuastionListCollectionViewSource(int NumberOfRows)
        {
            numberOfRows = NumberOfRows;
        }
         
        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (QuastionsListCell)collectionView.DequeueReusableCell("quastionListCell", indexPath);
            cell.UpdateCell(indexPath.Row + 1, collectionView);
            return cell;
        }
        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            
            return numberOfRows;
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }
        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
        
        }
    }
}