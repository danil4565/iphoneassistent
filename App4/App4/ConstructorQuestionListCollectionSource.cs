﻿using System;
using Foundation;
using UIKit;

namespace App4
{
    internal class ConstructorQuestionListCollectionSource : UICollectionViewSource
    {
        private UICollectionView answersCollectionView;

        AllCellsData cellsData;

        Int selectedIndex;
        public ConstructorQuestionListCollectionSource(UICollectionView answersCollectionView, AllCellsData allCellsData, Int selectedIndex)
        {
            this.selectedIndex = selectedIndex;
            this.answersCollectionView = answersCollectionView;
            cellsData = allCellsData;

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (ConstructorQuastionsListCell)collectionView.DequeueReusableCell(ConstructorQuastionsListCell.reuseIdentifier, indexPath);
            string indexStr = indexPath.Row == cellsData.questionCells.Count ? "+" : (indexPath.Row + 1).ToString();
            UIColor color = selectedIndex.intIndex == indexPath.Row ? new UIColor(red: 0.70f, green: 0.90f, blue: 0.99f, alpha: 1.0f) : null;
            cell.UpdateCell(indexStr, color);
            
            return cell;
        }
        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }
        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
            if (indexPath.Row == cellsData.questionCells.Count)
            {
                cellsData.questionCells.Add(new QuestionCellData(1));
                collectionView.InsertItems(new NSIndexPath[] { indexPath });
                answersCollectionView.InsertItems(new NSIndexPath[] { indexPath });
                collectionView.ScrollToItem(NSIndexPath.FromItemSection(indexPath.Row + 1, 0), UICollectionViewScrollPosition.Right, true);
            } 
            else
            {
                answersCollectionView.ScrollToItem(indexPath, UICollectionViewScrollPosition.Left, true);
            }
        }
        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return cellsData.questionCells.Count + 1;
        }
    }
}