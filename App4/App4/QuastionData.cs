﻿using Foundation;
using ICSharpCode.SharpZipLib.Zip;
using Ionic.Zlib;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using UIKit;

namespace App4
{
    public class Questions
    {
        public string Question;
        public bool MultipleAnswer = false;
        public int NumberOfRightAnswers = 0;
        public List<Answers> answers = new List<Answers>();
        public class Answers
        {
            public string Answer;
            public bool IsCorrect;
            public Answers(string Answer, bool IsCorrect)
            {
                this.Answer = Answer;
                this.IsCorrect = IsCorrect;
            }
        }
    }
    [Serializable]
    public class TestSettings
    {
        public int numOfQuestions { get; set; }
        public int timerMin { get; set; }
        public bool questionsMixed { get; set; }
        public bool answersMixed { get; set; }

    }
    //Класс содержащий все данные одного теста, реализующий обработку и систематезацию данных
    [Serializable]
    public class TestData
    {
        //public ViewController Viev;
        private string CachesAppPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Resources), "Caches", "MyTestApp");/* =Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);*/
        private string FilePathCopy;
        private string fileExtension;
        public string RightFileName;
        public string FileName;
        public string FilePath;
        public TestSettings TestSettings { get; set; }
        private string[] availableExtentions = { ".qst", ".txt", ".it2", ".qsz" };
        public string CreationDate;
        public readonly int NumberOfQuastions = 0;
        public readonly bool IsMultipleAnswersAvailable = false;

        public TestData(string FileName, string FilePath)
        {
            RightFileName = FileName.Remove(FileName.Length - 4);
            string extensionBuffer = "";
            foreach (char ch in FileName.TakeLast(4))
                extensionBuffer += ch;
            fileExtension = extensionBuffer;
            var date = NSDate.Now;
            var df = new NSDateFormatter();
            df.DateStyle = NSDateFormatterStyle.Medium;
            df.TimeStyle = NSDateFormatterStyle.Short;
            CreationDate = df.StringFor(date);
            this.FileName = FileName;
            this.FilePath = FilePath;
            Save();
            NumberOfQuastions = this.GetRightAnswers().Count;
            IsMultipleAnswersAvailable = this.AnswerVariants();


        }

        private void Save()
        {
            FilePathCopy = Path.Combine(CachesAppPath, FileName);
            Directory.CreateDirectory(CachesAppPath);
            string FilePathXml = Path.Combine(CachesAppPath, "test.xml");
            try
            {
                if (fileExtension == ".it2")
                {
                    var ParseXml = ReadXmlFile();
                    if (CheckFile(ParseXml))
                        File.WriteAllLines(FilePathCopy, ParseXml, Encoding.UTF8);

                    File.Delete(FilePath);
                }
                if (fileExtension == ".qst" || fileExtension == ".txt")
                {
                    if (CheckFile(ReadAllLineFile(FilePath)))
                        File.Copy(FilePath, FilePathCopy, true);
                    File.Delete(FilePath);

                }
                if (fileExtension == ".qsz")
                {

                    var ReadQsz = ReaQszFile();
                    if (CheckFile(ReadQsz))
                        File.WriteAllLines(FilePathCopy, ReadQsz, Encoding.GetEncoding(1251));
                    File.Delete(FilePath);
                }
            }
            catch { }
        }

        private List<string> ReaQszFile()
        {

            var resultArry = new List<string>();
            var data = File.ReadAllBytes(FilePath);
            data = ZlibStream.UncompressBuffer(data);
            var encodingFile = Encoding.GetEncoding(1251);
            int counter = 0;
            while (counter < data.Length)
            {
                int Length = BitConverter.ToInt32(data, counter);
                string Strings = encodingFile.GetString(data, counter + 4, Length);
                counter += Length + 8;
                int CheckAnswer = BitConverter.ToInt32(data, counter - 4);
                //NewArr.Add(new ReadQsz(Length, CheckAnswer, Strings));
                if (CheckAnswer > 0)
                {
                    resultArry.Add("?");
                    resultArry.Add(Strings);
                }
                if (CheckAnswer == 0)
                {
                    resultArry.Add($"-{Strings}");
                }
                if (CheckAnswer == -1)
                {
                    resultArry.Add($"+{Strings}");
                }
            }
            return resultArry;
        }

        private List<string> ReadXmlFile()
        {

            var FilePathXml = Path.Combine(CachesAppPath, "test.xml");
            FastZip fz = new FastZip();
            fz.ExtractZip(FilePath, CachesAppPath, "test.xml");
            List<string> ParseXml = new List<string>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(FilePathXml);
            File.Delete(FilePathXml);
            XmlNode xnode = xDoc.GetElementsByTagName("section")[0];
            XmlNode Questions = xnode.ChildNodes[0];
            foreach (XmlNode Question in Questions.ChildNodes)
            {
                foreach (XmlNode Content in Question.ChildNodes)
                {

                    if (Content.Name == "content")
                    {
                        XmlNode Text = Content.ChildNodes[0];
                        XmlNode NameQestion = Text.Attributes.GetNamedItem("value");
                        if (NameQestion != null)
                        {
                            ParseXml.Add("?");
                            ParseXml.Add(NameQestion.Value);
                        }

                    }
                    // если узел age
                    if (Content.Name == "choices")
                    {
                        foreach (XmlNode Choice in Content.ChildNodes)
                        {
                            try
                            {
                                XmlNode Contente = Choice.ChildNodes[0];
                                XmlNode Text = Contente.ChildNodes[0];
                                XmlNode Answer = Text.Attributes.GetNamedItem("value");
                                if (Answer != null)
                                {
                                    if (Choice.Attributes.GetNamedItem("correct").Value == "true")
                                        ParseXml.Add("+" + Answer.Value);
                                    else
                                    {
                                        ParseXml.Add("-" + Answer.Value);
                                    }

                                }
                            }
                            catch { }
                        }

                    }
                    if (Content.Name == "patterns" || Content.Name == "matchOptions" || Content.Name == "orderOptions" || Content.Name == "classifyOptions")
                    {
                        ParseXml.RemoveAt(ParseXml.Count - 1);
                        ParseXml.RemoveAt(ParseXml.Count - 1);
                    }
                }
            }
            return ParseXml;
        }
        private bool CheckFile(List<string> StringList)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {

                // Хреновый мнетод проверки 
                //if (StringList.Count > 4)
                //{
                //    if (StringList[0] == "?" && StringList[2][0] == '+' || StringList[2][0] == '-')
                //    {
                //        return true;
                //    }
                //}
                //Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("В тесте содержится ошибка, я не могу его прочитать :("));
                //return false;

                //    stopWatch.Stop();
                //    long time = stopWatch.ElapsedMilliseconds;
                //    return false;
                if (StringList.Count == (StringList.Count(Q => Q == "?") * 2) + StringList.Count(A => A[0] == '+') + StringList.Count(FA => FA[0] == '-'))
                {
                    stopWatch.Stop();
                    long times = stopWatch.ElapsedMilliseconds;
                    return true;
                }
                stopWatch.Stop();
                long time = stopWatch.ElapsedMilliseconds;
                Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("Невозможно прочитать тест"));
                return false;
            }
            catch
            {
                Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 2, 0.3, Localizable.Str("Невозможно почитать тест"));
                return false;
            }
        }
        public void RemoveFile()
        {
            try
            {
                File.Delete(FilePathCopy);
            }
            catch { }
        }
        public bool AnswerVariants()
        {
            List<List<string>> RightAnswers = GetRightAnswers();
            for (int i = 0; i < RightAnswers.Count; i++)
                if (RightAnswers[i].Count > 2)
                {
                    return true;
                }
            return false;
        }
        public void RenameFile()
        {
            try
            {
                string RenameFilePathCopy = Path.Combine(CachesAppPath, RightFileName + ".qst");
                File.Move(FilePathCopy, RenameFilePathCopy);
                RemoveFile();
                FilePathCopy = RenameFilePathCopy;
            }
            catch { }
        }

        private Encoding GetEncodingFile(string Path)
        {
            using (FileStream fs = File.OpenRead(Path))
            {
                Ude.CharsetDetector cdet = new Ude.CharsetDetector();
                cdet.Feed(fs);
                cdet.DataEnd();
                try
                {
                    return Encoding.GetEncoding(cdet.Charset);
                }
                catch (Exception)
                {
                    return Encoding.Default;
                }
            }
        }
        private List<string> ReadAllLineFile(string Path)
        {
            return File.ReadAllLines(Path, GetEncodingFile(Path)).ToList();
        }

        //Возвращает двумерный массив с правилные ответы и вопросами , принимает чистый массив стрингов 
        public List<List<string>> GetRightAnswers()
        {
            List<List<string>> SortedString = new List<List<string>>();
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var StringList = ReadAllLineFile(FilePathCopy);
            for (int l = 0, number = 1; l < StringList.Count; l++)
            {
                if (StringList[l] == "?")
                {
                    StringList[l + 1] = $"#{number} {StringList[l + 1]}";
                    StringList.RemoveAt(l);
                    number++;
                }
            }
            for (int i = 0, questionNum = -1, answerNum = 1; i < StringList.Count; i++)
            {
                if (StringList[i][0] == '#')
                {
                    questionNum++;
                    answerNum = 1;
                    SortedString.Add(new List<string>());
                    SortedString[questionNum].Add(StringList[i]);

                }
                else if (StringList[i][0] == '+')
                {
                    SortedString[questionNum].Add(StringList[i].Replace("+", $"{answerNum}) "));
                    answerNum++;
                }
            }
            stopWatch.Stop();
            long time = stopWatch.ElapsedMilliseconds;
            return SortedString;

        }

        //Возврашает двумерный массив где есть  ответы и вопрос , принимает чистый массив стрингов
        public List<Questions> GetAllAnswers(bool IsMixed = false, bool IsAnswersMixe = false, bool Numeration = true)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            Random rand = new Random(DateTime.Now.Millisecond);
            var SortString = new List<Questions>();
            var StringList = ReadAllLineFile(FilePathCopy);
            Questions Questions = new Questions();
          

            for (int l = 0; l < StringList.Count; l++)
            {
                if (StringList[l] == "?")
                {
                    StringList[l + 1] = $"#{StringList[l + 1]}";
                    StringList.RemoveAt(l);
                }
            }
            for (int i = 0, NumberOfRightAnswers = 0 ; i < StringList.Count; i++)
            {
                if (StringList[i][0] == '#')
                {
                    NumberOfRightAnswers = 0;
                    Questions = new Questions();
                    Questions.Question = StringList[i];
                    //SortString.Add(new List<Questions>());
                    SortString.Add(Questions);
                }
                else if (StringList[i][0] == '+')
                {
                    //SortString[questionNum].Add(new Questions(StringList[i].Replace("+", ""), true));
                    Questions.answers.Add(new Questions.Answers(StringList[i].Replace("+", ""), true));
                    NumberOfRightAnswers++;
                    Questions.NumberOfRightAnswers = NumberOfRightAnswers;
                    if (NumberOfRightAnswers == 2)
                    {
                        Questions.MultipleAnswer = true;
                    }
                }
                else if (StringList[i][0] == '-')
                {
                    //SortString[questionNum].Add(new Questions(StringList[i].Replace("-", ""), false));
                    Questions.answers.Add(new Questions.Answers(StringList[i].Replace("-", ""), false));
                }

            }
            if (IsMixed)
            {
                SortString = SortString.OrderBy(x => rand.Next()).ToList();
            }
            if (IsAnswersMixe)
            {
                for (int l = 0; l < SortString.Count; l++)
                {
                    SortString[l].answers = SortString[l].answers.OrderBy(x => rand.Next(0, SortString[l].answers.Count - 1)).ToList();   /*Take(1).Union(SortString[l].Skip(1).OrderBy(x => rand.Next())).ToList();*/
                }
            }
            if (Numeration)
            {
                for (int l = 0; l < SortString.Count; l++)
                {
                    SortString[l].Question = SortString[l].Question.Replace("#", $"#{l + 1} ");
                }
            }
            if (!Numeration)
            {
                for (int l = 0; l < SortString.Count; l++)
                {
                    SortString[l].Question = SortString[l].Question.Replace("#", $"");
                }
            }
            stopWatch.Stop();
            long time = stopWatch.ElapsedMilliseconds;
            return SortString;
        }
        //Проверка на допустимый тип (не используется)
        public bool IsAvailable => availableExtentions.Contains(fileExtension);
        
    }
}
