using Foundation;
using System;
using System.Collections.Generic;
using UIKit;

namespace App4
{
    public partial class AllAnswersTableCell : UITableViewCell
    {
        public AllAnswersTableCell (IntPtr handle) : base (handle)
        {  
        }
        public void UpdateCell(Questions list)
        {
            Quastian.Text = list.Question;
            AllAnswers.Text = "";
            for (int i = 0; i < list.answers.Count; i++)
            {
                AllAnswers.Text += $"{i+1})" + (list.answers[i].IsCorrect ? " + ": " - ") + list.answers[i].Answer + "\n";
            }

        }
    }
}