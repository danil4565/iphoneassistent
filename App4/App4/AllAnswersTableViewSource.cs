﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    class AllAnswersTableViewSource : UITableViewSource
    {
        TestData testData;
        List<Questions> quastions;
 

        public AllAnswersTableViewSource(TestData test)
        {
            testData = test;
            quastions = testData.GetAllAnswers();
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            
            var cell = (AllAnswersTableCell)tableView.DequeueReusableCell("allAnswersTableCell", indexPath);
            
            try
            {
                cell.UpdateCell(quastions[indexPath.Row]);
            }
            catch { }


            return cell;

        }
        
        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return testData.NumberOfQuastions;
        }

    }
}
