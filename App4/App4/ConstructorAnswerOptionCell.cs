﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CoreGraphics;
using Foundation;
using SaturdayMP.XPlugins.iOS;
using UIKit;

namespace App4
{
    class ConstructorAnswerOptionCell : UITableViewCell
    {

        public static string reuseIdentifier { get; } = "constructorAnswerOptionCell";
        public BEMCheckBox answerRadio;
        public UITextView answerText;
        UIButton deleteButton;
        string placeholder = Localizable.Str("Ответ");

        UITableView main;
        NSIndexPath index;
        AllCellsData cellsData;
        int itemIndex;

        protected internal ConstructorAnswerOptionCell(IntPtr handle) : base(handle)
        {
            answerRadio = new BEMCheckBox(new CoreGraphics.CGRect(0, 0, 24, 24));
            answerText = new UITextView(new CoreGraphics.CGRect(0, 0, 10, 30));
            deleteButton = new UIButton(UIButtonType.Custom);

            deleteButton.SetImage(UIImage.FromFile("Delete.png"), UIControlState.Normal);

            AddSubview(deleteButton);

            deleteButton.TouchDown += (e, a) =>
            {
                UIView.Animate(0.20, () =>
                {
                    deleteButton.Transform = CGAffineTransform.MakeScale(0.9f, 0.9f);
                    deleteButton.Alpha = 0.6f;
                });
            };

            deleteButton.TouchUpInside += async (e, a) =>
            {
                main.ReloadData();
                await Task.Delay(4);
                cellsData[itemIndex].answers.RemoveAt(index.Row);
                UIView.Animate(0.23, () =>
                {
                    deleteButton.Transform = CGAffineTransform.MakeIdentity();
                    deleteButton.Alpha = 1f;
                });
                main.DeleteRows(new NSIndexPath[] { index }, UITableViewRowAnimation.Top);

            };

            deleteButton.TouchDragExit += (e, a) =>
            {
                UIView.Animate(0.15, () =>
                {
                    deleteButton.Transform = CGAffineTransform.MakeIdentity();
                    deleteButton.Alpha = 1f;
                });
            };

            deleteButton.ImageEdgeInsets = new UIEdgeInsets(5, 5, 5, 5);

            deleteButton.Layer.BorderColor = UIColor.Gray.CGColor;
            deleteButton.Layer.BorderWidth = (nfloat)1.4;
            deleteButton.Layer.CornerRadius = (nfloat)5.0;

            deleteButton.TranslatesAutoresizingMaskIntoConstraints = false;

            deleteButton.RightAnchor.ConstraintEqualTo(RightAnchor, -15).Active = true;
            deleteButton.TopAnchor.ConstraintEqualTo(TopAnchor, 8).Active = true;
            deleteButton.WidthAnchor.ConstraintEqualTo(29).Active = true;
            deleteButton.HeightAnchor.ConstraintEqualTo(29).Active = true;

            answerRadio.OnTintColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor;
            answerRadio.OnCheckColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor;
            answerRadio.LineWidth = 2;
            answerRadio.BoxType = BEMBoxType.Square;
            answerRadio.OffAnimationType = BEMAnimationType.Fade;
            answerRadio.OnAnimationType = BEMAnimationType.Fade;
            

            answerRadio.AnimationDidStopForCheckBox += (e, a) =>
            {
                cellsData[itemIndex][index.Row].IsCorrect = answerRadio.On;
            };

            answerText.Font = UIFont.SystemFontOfSize(18);

            

            AddSubview(answerRadio);
            AddSubview(answerText);





            answerRadio.TranslatesAutoresizingMaskIntoConstraints = false;
            answerRadio.TopAnchor.ConstraintEqualTo(TopAnchor, 10).Active = true;
            answerRadio.LeftAnchor.ConstraintEqualTo(LeftAnchor, 15).Active = true;

            answerText.TranslatesAutoresizingMaskIntoConstraints = false;
            answerText.LeftAnchor.ConstraintEqualTo(answerRadio.RightAnchor, 8).Active = true;
            answerText.RightAnchor.ConstraintEqualTo(deleteButton.LeftAnchor, -5).Active = true;
            answerText.BottomAnchor.ConstraintEqualTo(BottomAnchor, -5).Active = true;
            answerText.TopAnchor.ConstraintEqualTo(TopAnchor, 8).Active = true;

            answerText.ScrollEnabled = false;
            answerText.SizeToFit();

            answerText.Layer.BorderColor = UIColor.Gray.CGColor;
            answerText.Layer.BorderWidth = (nfloat)1.4;
            answerText.Layer.CornerRadius = (nfloat)5.0;
            answerText.TextContainerInset = new UIEdgeInsets((nfloat)3, (nfloat)4, (nfloat)5, (nfloat)4);
            //answerText.TextContainer.LineFragmentPadding = 0;
            answerText.Text = placeholder;
            answerText.TextColor = UIColor.LightGray;

            answerText.ShouldBeginEditing = (t) =>
            {
                if (t.TextColor == UIColor.LightGray)
                {
                    t.Text = "";
                    t.TextColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor;
                }
                return true;
            };
            answerText.ShouldEndEditing = (t) =>
            {
                if (t.Text.Length == 0 || t.Text == null)
                {
                    t.Text = placeholder;
                    t.TextColor = UIColor.LightGray;
                }
                return true;
            };
            answerText.Changed +=
            (e, a) =>
            {
                cellsData[itemIndex][index.Row].Text = answerText.Text;
                main.BeginUpdates();
               
                main.EndUpdates();
            };
            

        }
        public void UpdateCell(UITableView tableView, NSIndexPath indexPath, AllCellsData cellsData, int itemIndex)
        {
            answerRadio.On = cellsData[itemIndex][indexPath.Row].IsCorrect;
            this.itemIndex = itemIndex;
            if (cellsData[itemIndex][indexPath.Row].Text != null && cellsData[itemIndex][indexPath.Row].Text.Length != 0)
            {
                answerText.TextColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor;
                answerText.Text = cellsData[itemIndex][indexPath.Row].Text;
            }
            else
            {
                answerText.Text = placeholder;
                answerText.TextColor = UIColor.LightGray;
            }
            this.cellsData = cellsData;
            main = tableView;
            index = indexPath;
        }




    }
}
