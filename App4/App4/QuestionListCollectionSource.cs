﻿//#define RELEASE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;

namespace App4
{
    class QuestionListCollectionSource : UICollectionViewSource
    {
        UICollectionView answersCollection;
        int numberOfQuestions;
        List<UIColor> colors;
        AllCellsData cellsData;

        public QuestionListCollectionSource(int numberOfQuestions, UICollectionView collectionView, List<UIColor> colors, AllCellsData cellsData)
        {
            this.cellsData = cellsData;
            this.colors = colors;
            answersCollection = collectionView;
            this.numberOfQuestions = numberOfQuestions;
        }
        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (QuastionsListCell)collectionView.DequeueReusableCell(QuastionsListCell.reuseIdentifier, indexPath);
            cell.UpdateCell(indexPath.Row + 1, colors[indexPath.Row]);
            return cell;
       
        }
        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }
        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {
#if RELEASE

            answersCollection.ScrollToItem(NSIndexPath.FromItemSection(indexPath.Row, 0), UICollectionViewScrollPosition.Left, true);
            

#else
            answersCollection.ScrollToItem(NSIndexPath.FromItemSection(indexPath.Row, 0), UICollectionViewScrollPosition.Left, false);
            UIView.Transition(answersCollection, 0.6, UIViewAnimationOptions.TransitionCrossDissolve, () => { answersCollection.ReloadData(); }, null);
#endif
        }
        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return numberOfQuestions;
        }
    }
}