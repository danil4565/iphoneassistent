// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("MainTableCell")]
    partial class MainTableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel CreationDate { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel MultipleAnswers { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NumberOfQuestions { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel TestName { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (CreationDate != null) {
                CreationDate.Dispose ();
                CreationDate = null;
            }

            if (MultipleAnswers != null) {
                MultipleAnswers.Dispose ();
                MultipleAnswers = null;
            }

            if (NumberOfQuestions != null) {
                NumberOfQuestions.Dispose ();
                NumberOfQuestions = null;
            }

            if (TestName != null) {
                TestName.Dispose ();
                TestName = null;
            }
        }
    }
}