﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SaturdayMP.XPlugins.iOS;


namespace App4
{
    class ExtentionTableCell : UITableViewCell
    {
        public static string ReuseIndentifier { get; } = "extentionTableCell";
        BEMCheckBox extentionRadio;
        UILabel extentionLabel;

        public ExtentionTableCell(IntPtr handle) : base(handle)
        {
            extentionRadio = new BEMCheckBox(new CoreGraphics.CGRect(0, 0, 23, 23));
            extentionLabel = new UILabel();
            AddSubview(extentionLabel);

            extentionRadio.OnTintColor = UIColor.Black;
            extentionRadio.OnCheckColor = UIColor.Black;
            extentionRadio.LineWidth = 2;
            extentionRadio.BoxType = BEMBoxType.Circle;
            extentionRadio.OffAnimationType = BEMAnimationType.Fade;
            extentionRadio.OnAnimationType = BEMAnimationType.Fade;

            extentionLabel.LineBreakMode = UILineBreakMode.WordWrap;
            extentionLabel.Lines = 0;
            extentionLabel.UserInteractionEnabled = true;

            var tap = new UITapGestureRecognizer(() =>
            {
                extentionRadio.SetOn(true, true);
            });
            extentionLabel.AddGestureRecognizer(tap);


            extentionRadio.AnimationDidStopForCheckBox += (e, a) =>
            {
            };

            AddSubview(extentionRadio);
            const int additionalDistanceFromTop = 3;




            extentionRadio.TranslatesAutoresizingMaskIntoConstraints = false;
            extentionRadio.TopAnchor.ConstraintEqualTo(TopAnchor, 7 + additionalDistanceFromTop).Active = true;
            extentionRadio.LeftAnchor.ConstraintEqualTo(LeftAnchor, 10).Active = true;


            extentionLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            extentionLabel.TopAnchor.ConstraintEqualTo(this.TopAnchor, 8 + additionalDistanceFromTop).Active = true;
            extentionLabel.LeftAnchor.ConstraintEqualTo(extentionRadio.LeftAnchor, 35).Active = true;
            extentionLabel.RightAnchor.ConstraintEqualTo(this.RightAnchor, -5).Active = true;
            extentionLabel.BottomAnchor.ConstraintEqualTo(this.BottomAnchor, -8).Active = true;
        }

        internal void UpdateCell(BEMCheckBoxGroup group, NSIndexPath indexPath, string extention)
        {
            group.AddCheckBoxToGroup(extentionRadio);
            
        }


    }
}