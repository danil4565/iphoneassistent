﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Foundation;
using UIKit;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace App4
{
    public  class SerializeExtension
    { 
        private static string CachesAppPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Resources), "Caches","MyTestApp");
        

        public static void Serialize(List<TestData> yourlist)
        {
            try
            {
                Stream SaveFileStream = File.Create(Path.Combine(CachesAppPath, "Save.bin"));
                BinaryFormatter serializer = new BinaryFormatter();
                serializer.Serialize(SaveFileStream, yourlist);
                SaveFileStream.Close();
            }
            catch
            {

            }
            
           
                                                 
        }


        public static List<TestData> Deserialize()
        {
            List<TestData> data = new List<TestData>();
            try
            {
              
                Stream SaveFileStream = File.OpenRead(Path.Combine(CachesAppPath, "Save.bin"));
                BinaryFormatter serializer = new BinaryFormatter();
                data = (List<TestData>)serializer.Deserialize(SaveFileStream);
                SaveFileStream.Close();
                return data;
            }
            catch
            {
                return data;
            }
        }
    }
}
