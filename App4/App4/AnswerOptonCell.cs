using Foundation;
using System;
using UIKit;
using Syncfusion.iOS.Buttons;
using CoreGraphics;

namespace App4
{
    public partial class AnswerOptonCell : UICollectionViewCell
    {
        public AnswerOptonCell(IntPtr handle) : base(handle)
        {


        }
        public void UpdateCell(string answer, SfRadioGroup radioGroup)
        {   
            BackgroundColor = UIColor.Black;
            SfRadioButton radioButton = new SfRadioButton();
            radioButton.TitleLabel.Text = answer;
            radioGroup.AddArrangedSubview(radioButton);
            radioButton.Frame = new CoreGraphics.CGRect(5, 5, 100, 20);
            AddSubview(radioButton);
        }
        
    }
}