﻿// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem AddTestButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIBarButtonItem CreateTestButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UINavigationItem MainNavigItem { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITableView MainTableView { get; set; }

        [Action ("AddTestButton_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void AddTestButton_Activated (UIKit.UIBarButtonItem sender);

        [Action ("CreateTestButton_Activated:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void CreateTestButton_Activated (UIKit.UIBarButtonItem sender);

        void ReleaseDesignerOutlets ()
        {
            if (AddTestButton != null) {
                AddTestButton.Dispose ();
                AddTestButton = null;
            }

            if (CreateTestButton != null) {
                CreateTestButton.Dispose ();
                CreateTestButton = null;
            }

            if (MainNavigItem != null) {
                MainNavigItem.Dispose ();
                MainNavigItem = null;
            }

            if (MainTableView != null) {
                MainTableView.Dispose ();
                MainTableView = null;
            }
        }
    }
}