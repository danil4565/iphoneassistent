﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using Syncfusion.iOS.Buttons;

namespace App4
{
    class AllAnswersCollectionViewSource: UICollectionViewSource
    {
        SfRadioGroup groupOfButtons;
        public AllAnswersCollectionViewSource()
        {
            groupOfButtons = new SfRadioGroup();
        }
        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (AnswerOptonCell)collectionView.DequeueReusableCell("answerOptonCell", indexPath);
            cell.UpdateCell("sdf", groupOfButtons);
            
            return cell;
            
        }
        
        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {

            return 10;
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }
        public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
        {

        }
    }
}