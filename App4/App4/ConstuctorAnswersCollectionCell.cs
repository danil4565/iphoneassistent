﻿using UIKit;
using Foundation;
using System;
using CoreGraphics;

namespace App4
{
    internal class ConstructorAnswersCollectionCell : UICollectionViewCell
    {
        public static string reuseIdentifier { get; } = "constructorAnswersCollectionCell";

        public UITableView answersTable;
        UITextView questionText;
        AllCellsData cellsData;
        int index;
        string placeholder = Localizable.Str("Вопрос");

        public ConstructorAnswersCollectionCell(IntPtr handle) : base(handle)
        {



            questionText = new UITextView();

            questionText.Font = UIFont.SystemFontOfSize(18);


            AddSubview(questionText);

            questionText.TranslatesAutoresizingMaskIntoConstraints = false;

            questionText.TopAnchor.ConstraintEqualTo(SafeAreaLayoutGuide.TopAnchor, 5).Active = true;
            questionText.LeftAnchor.ConstraintEqualTo(SafeAreaLayoutGuide.LeftAnchor, 15).Active = true;
            questionText.RightAnchor.ConstraintEqualTo(SafeAreaLayoutGuide.RightAnchor, -15).Active = true;
            questionText.HeightAnchor.ConstraintEqualTo(87).Active = true;

            questionText.TextContainerInset = UIEdgeInsets.Zero;
            questionText.TextContainer.LineFragmentPadding = 0;

            questionText.Layer.BorderColor = UIColor.Gray.CGColor;
            questionText.Layer.BorderWidth = (nfloat)1.4;
            questionText.Layer.CornerRadius = (nfloat)5.0;
            questionText.TextContainerInset = new UIEdgeInsets((nfloat)1, (nfloat)4, (nfloat)5, (nfloat)4);

            answersTable = new UITableView();
            answersTable.SeparatorStyle = UITableViewCellSeparatorStyle.None;
            answersTable.RowHeight = UITableView.AutomaticDimension;
            answersTable.EstimatedRowHeight = 40;
            answersTable.RegisterClassForCellReuse(typeof(ConstructorAnswerOptionCell), ConstructorAnswerOptionCell.reuseIdentifier);
            answersTable.RegisterClassForCellReuse(typeof(ConstructorAnswerOptionAddCell), ConstructorAnswerOptionAddCell.reuseIdentifier);
            AddSubview(answersTable);

            answersTable.TranslatesAutoresizingMaskIntoConstraints = false;

            answersTable.TopAnchor.ConstraintEqualTo(questionText.BottomAnchor, 5).Active = true;
            answersTable.LeftAnchor.ConstraintEqualTo(LeftAnchor).Active = true;
            answersTable.RightAnchor.ConstraintEqualTo(RightAnchor).Active = true;
            answersTable.BottomAnchor.ConstraintEqualTo(BottomAnchor).Active = true;
#if RELEASE
            answersTable.AlwaysBounceVertical = false;
#endif
            questionText.Text = placeholder;
            questionText.TextColor = UIColor.LightGray;
            questionText.ShouldBeginEditing = (t) => 
            {
                if (t.TextColor == UIColor.LightGray)
                {
                    t.Text = "";
                    t.TextColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor; ;
                }
                return true;
            };
            questionText.ShouldEndEditing = (t) =>
            {
                if (t.Text.Length == 0 || t.Text == null)
                {
                    t.Text = placeholder;
                    t.TextColor = UIColor.LightGray;
                }
                return true;
            };
            questionText.Changed +=
            (e, a) =>
            {
                cellsData[index].Question = questionText.Text;
            };
        }

        public void UpdateCell(AllCellsData cellsData, int itemIndex)
        {
            index = itemIndex;
            this.cellsData = cellsData;
            if (cellsData[itemIndex].Question != null && cellsData[itemIndex].Question.Length != 0)
            {
                questionText.TextColor = IsDark.Dark ? UIColor.LightTextColor : UIColor.DarkTextColor; ;
                questionText.Text = cellsData[itemIndex].Question;
            }
            else
            {
                questionText.TextColor = UIColor.LightGray;
                questionText.Text = placeholder;
            }
            answersTable.Source = new ConstructorAnswersOptionsTableSource(cellsData, itemIndex);
        }



    }
}
