// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("AllAnswersTableCell")]
    partial class AllAnswersTableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel AllAnswers { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Quastian { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (AllAnswers != null) {
                AllAnswers.Dispose ();
                AllAnswers = null;
            }

            if (Quastian != null) {
                Quastian.Dispose ();
                Quastian = null;
            }
        }
    }
}