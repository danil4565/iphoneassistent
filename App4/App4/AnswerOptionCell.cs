﻿using System;
using System.Threading;

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SaturdayMP.XPlugins.iOS;


namespace App4
{
    class AnswerOptionCell : UITableViewCell
    {
        public static string reuseIndentifier { get; } = "answerOptionCell";
        public BEMCheckBox answerRadio;
        public UILabel answerDescription;
        bool FirstUpdate = true;

        AllCellsData cellsData;

        int collectionCellId;
        int tableCellId;

        public AnswerOptionCell(IntPtr handle) : base(handle)
        {

            answerRadio = new BEMCheckBox(new CoreGraphics.CGRect(0, 0, 23, 23));
            answerDescription = new UILabel();
            AddSubview(answerDescription);

            answerRadio.OnTintColor = UIColor.Black;
            answerRadio.OnCheckColor = UIColor.Black;
            answerRadio.LineWidth = 2;
            answerRadio.BoxType = BEMBoxType.Circle;
            answerRadio.OffAnimationType = BEMAnimationType.Fade;
            answerRadio.OnAnimationType = BEMAnimationType.Fade;
            
            answerDescription.LineBreakMode = UILineBreakMode.WordWrap;
            answerDescription.Lines = 0;
            answerDescription.UserInteractionEnabled = true;

            var tap = new UITapGestureRecognizer(()=>
            {
                answerRadio.SetOn(true, true);
            });
            answerDescription.AddGestureRecognizer(tap);

            
            answerRadio.AnimationDidStopForCheckBox += (e, a) =>
            {
                cellsData[collectionCellId][tableCellId].IsChecked = answerRadio.On;
            };

            AddSubview(answerRadio);
            const int additionalDistanceFromTop = 3;




            answerRadio.TranslatesAutoresizingMaskIntoConstraints = false;
            answerRadio.TopAnchor.ConstraintEqualTo(TopAnchor, 7 + additionalDistanceFromTop).Active = true;
            answerRadio.LeftAnchor.ConstraintEqualTo(LeftAnchor, 10).Active = true;

            
            answerDescription.TranslatesAutoresizingMaskIntoConstraints = false;

            answerDescription.TopAnchor.ConstraintEqualTo(this.TopAnchor, 8 + additionalDistanceFromTop).Active = true;
            answerDescription.LeftAnchor.ConstraintEqualTo(answerRadio.LeftAnchor, 35).Active = true;
            answerDescription.RightAnchor.ConstraintEqualTo(this.RightAnchor, -5).Active = true;
            answerDescription.BottomAnchor.ConstraintEqualTo(this.BottomAnchor, -8).Active = true;
        }

        public void UpdateCell(string AnswerText, AllCellsData cellsData, BEMCheckBoxGroup radioGroup, int collectionCellId, int tableCellId)
        {
            this.cellsData = cellsData;
            this.collectionCellId = collectionCellId;
            this.tableCellId = tableCellId;

            if (answerRadio.On != cellsData[collectionCellId][tableCellId].IsChecked)
                answerRadio.On = cellsData[collectionCellId][tableCellId].IsChecked;

            this.UserInteractionEnabled = cellsData[collectionCellId].UserInteraction;
            this.BackgroundColor = cellsData[collectionCellId][tableCellId]?.backgroundColor;


            answerDescription.Text = AnswerText;

            if (FirstUpdate)
            {
                radioGroup.AddCheckBoxToGroup(answerRadio);
            }


            FirstUpdate = false;
        }

    }
}
