﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using UIKit;
using SaturdayMP.XPlugins.iOS;

namespace App4
{
    class ExtentionTableSuorce : UITableViewSource
    {
        public Int selected = new Int();
        public BEMCheckBoxGroup group = new BEMCheckBoxGroup();
        string[] extenions;

        public ExtentionTableSuorce(string[] extenions)
        {
            this.extenions = extenions;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (ExtentionTableCell)tableView.DequeueReusableCell(ExtentionTableCell.ReuseIndentifier, indexPath);
            cell.UpdateCell(group, indexPath, extenions[indexPath.Row]);
            return cell;
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return extenions.Length;
        }
        public class Int { public int selectedIndex = 0; };
    }
}