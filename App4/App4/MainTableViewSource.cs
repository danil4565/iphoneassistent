﻿//#define RELEASE_ANIMATIONS
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
//using SaturdayMP.XPlugins.iOS;
using UIKit;

namespace App4
{
    class MainTableViewSource : UITableViewSource
    {
        public List<TestData> testData;
        UIViewController controller;
       


        public MainTableViewSource(List<TestData> tests, UIViewController controller)
        {

            testData = tests;
            this.controller = controller;
           

        }
        
        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            var cell = (MainTableCell)tableView.DequeueReusableCell("mainTableCell", indexPath);
            if (cell == null)
            {
                cell = new MainTableCell((NSString)"mainTableCell");
            }
            cell.UpdateCell(testData[indexPath.Row]);
            return cell;
        }

        public override void RowSelected(UITableView tableView, NSIndexPath indexPath)
        {
            
            {//Всплывающее окно при нажатии. Доделать после решения проблемы
                var alert = UIAlertController.Create(null, null, UIAlertControllerStyle.Alert);
                alert.AddAction(UIAlertAction.Create(NSBundle.MainBundle.GetLocalizedString(Localizable.Str("Просмотр"), null), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    AllAnswersViewController previewViewController = controller.Storyboard.InstantiateViewController("AllAnswersView") as AllAnswersViewController;
                    previewViewController.test = testData[indexPath.Row];
                    controller.NavigationController.PushViewController(previewViewController, true);

                }));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Просмотр правильных ответов"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    RightAnswersViewController rightAnswersViewController = controller.Storyboard.InstantiateViewController("RightAnswersView") as RightAnswersViewController;
                    rightAnswersViewController.test = testData[indexPath.Row];
                    controller.NavigationController.PushViewController(rightAnswersViewController, true);
                }));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Тренировочный тест"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    //Create Alert
                    var examAlert = UIAlertController.Create(Localizable.Str("Тренировочный тест"), null, UIAlertControllerStyle.Alert);


                    UILabel numberOfQuestionsLabel = new UILabel();
                    UILabel timerLabel = new UILabel();
                    numberOfQuestionsLabel.Lines = 0;
                    numberOfQuestionsLabel.Text = Localizable.Str("Количество \nвопросов");
                    timerLabel.Text = Localizable.Str("Таймер(мин)");

                    examAlert.View.AddSubview(numberOfQuestionsLabel);
                    examAlert.View.AddSubview(timerLabel);

                    examAlert.View.TranslatesAutoresizingMaskIntoConstraints = false;

                    examAlert.View.HeightAnchor.ConstraintEqualTo(260).Active = true;

                    numberOfQuestionsLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                    numberOfQuestionsLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;
                    numberOfQuestionsLabel.TopAnchor.ConstraintEqualTo(examAlert.View.TopAnchor, 50).Active = true;


                    timerLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                    timerLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;
                    timerLabel.TopAnchor.ConstraintEqualTo(numberOfQuestionsLabel.BottomAnchor, 5).Active = true;

                    UITextField numberOfQuestions = new UITextField();
                    UITextField timer = new UITextField();

                    numberOfQuestions.Text = testData[indexPath.Row]?.TestSettings?.numOfQuestions == 0 ? "" : testData[indexPath.Row]?.TestSettings?.numOfQuestions.ToString();
                    timer.Text = testData[indexPath.Row]?.TestSettings?.timerMin == 0 ? "" : testData[indexPath.Row]?.TestSettings?.timerMin.ToString();
                    numberOfQuestions.ShouldChangeCharacters = (textField, range, replacement) =>
                    {
                        var newContent = new NSString(textField.Text).Replace(range, new NSString(replacement)).ToString();
                        int? parsed;

                        bool isBiggerAndNum = false;
                        try
                        {
                            if (replacement.Length != 0)
                            {
                                parsed = int.Parse(newContent);
                                isBiggerAndNum = parsed == null ? false : parsed <= testData[indexPath.Row].NumberOfQuastions;
                            }
                        }
                        catch
                        { }
                        return isBiggerAndNum || replacement.Length == 0;
                    };

                    timer.ShouldChangeCharacters = (textField, range, replacement) =>
                    {
                        var newContent = new NSString(textField.Text).Replace(range, new NSString(replacement)).ToString();
                        int number;
                        return newContent.Length <= 3 && (replacement.Length == 0 || int.TryParse(replacement, out number));
                    };

                    numberOfQuestions.Placeholder = Localizable.Str("Все");
                    timer.Placeholder = Localizable.Str("Нет");

                    //numberOfQuestions.SecureTextEntry = true;
                    numberOfQuestions.TintColor = UIColor.Black;
                    numberOfQuestions.BorderStyle = UITextBorderStyle.RoundedRect;

                    timer.TintColor = UIColor.Black;
                    timer.BorderStyle = UITextBorderStyle.RoundedRect;


                    examAlert.View.AddSubview(numberOfQuestions);
                    examAlert.View.AddSubview(timer);

                    numberOfQuestions.TranslatesAutoresizingMaskIntoConstraints = false;

                    numberOfQuestions.HeightAnchor.ConstraintEqualTo(30).Active = true;
                    numberOfQuestions.WidthAnchor.ConstraintEqualTo(75).Active = true;
                    numberOfQuestions.TopAnchor.ConstraintEqualTo(examAlert.View.TopAnchor, 50).Active = true;
                    numberOfQuestions.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                    timer.TranslatesAutoresizingMaskIntoConstraints = false;

                    timer.HeightAnchor.ConstraintEqualTo(30).Active = true;
                    timer.WidthAnchor.ConstraintEqualTo(75).Active = true;
                    timer.TopAnchor.ConstraintEqualTo(numberOfQuestions.BottomAnchor, 10).Active = true;
                    timer.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                    UISwitch mixeQuestionsSwith = new UISwitch();
                    UISwitch mixeAnsersSwith = new UISwitch();

                    mixeAnsersSwith.On = testData[indexPath.Row]?.TestSettings?.answersMixed ?? false;
                    mixeQuestionsSwith.On = testData[indexPath.Row]?.TestSettings?.questionsMixed ?? false;

                    examAlert.View.AddSubview(mixeAnsersSwith);
                    examAlert.View.AddSubview(mixeQuestionsSwith);

                    mixeQuestionsSwith.TranslatesAutoresizingMaskIntoConstraints = false;

                    mixeQuestionsSwith.TopAnchor.ConstraintEqualTo(timer.BottomAnchor, 10).Active = true;
                    mixeQuestionsSwith.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                    mixeAnsersSwith.TranslatesAutoresizingMaskIntoConstraints = false;

                    mixeAnsersSwith.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;
                    mixeAnsersSwith.TopAnchor.ConstraintEqualTo(mixeQuestionsSwith.BottomAnchor, 10).Active = true;

                    UILabel mixeQuestionsLabel = new UILabel();
                    UILabel mixeAnswersLabel = new UILabel();

                    mixeQuestionsLabel.Text = Localizable.Str("Перемешать вопросы");
                    mixeAnswersLabel.Text = Localizable.Str("Перемешать ответы");

                    examAlert.View.AddSubview(mixeQuestionsLabel);
                    examAlert.View.AddSubview(mixeAnswersLabel);

                    mixeQuestionsLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                    mixeQuestionsLabel.TopAnchor.ConstraintEqualTo(timerLabel.BottomAnchor, 17).Active = true;
                    mixeQuestionsLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;


                    mixeAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                    mixeAnswersLabel.TopAnchor.ConstraintEqualTo(mixeQuestionsLabel.BottomAnchor, 20).Active = true;
                    mixeAnswersLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;


                    var cancelAction = UIAlertAction.Create(Localizable.Str("Отмена"), UIAlertActionStyle.Cancel, null);
                    var okayAction = UIAlertAction.Create(Localizable.Str("Ок"), UIAlertActionStyle.Default,
                        (e) =>
                        {
                            try
                            {
                                int numOfQuestions = 0;
                                int.TryParse(numberOfQuestions.Text, out numOfQuestions);

                                bool MixeArr = (numOfQuestions == 0) || (numOfQuestions == testData[indexPath.Row].NumberOfQuastions) ? mixeQuestionsSwith.On : true;

                                

                                TestViewController newBeta = new TestViewController();
                                newBeta.questionsList = testData[indexPath.Row].GetAllAnswers(MixeArr, mixeAnsersSwith.On,true).
                                    Take(numOfQuestions == 0 ? testData[indexPath.Row].NumberOfQuastions : numOfQuestions).ToList();
                                newBeta.testData = testData[indexPath.Row];
                                long minutes = 0;
                                long.TryParse(timer.Text,out minutes);
                                newBeta.secondsForTimer = minutes * 60;
                                controller.NavigationController.PushViewController(newBeta, true);

                                testData[indexPath.Row].TestSettings = new TestSettings()
                                {
                                    questionsMixed = mixeQuestionsSwith.On,
                                    answersMixed = mixeAnsersSwith.On,
                                    numOfQuestions = numOfQuestions,
                                    timerMin = (int)minutes
                                };
                                SerializeExtension.Serialize(testData);

                            }
                            catch
                            {
                                var alert2 = UIAlertController.Create(Localizable.Str("Ошибка!"), null, UIAlertControllerStyle.Alert);
                                var atr = new NSAttributedString(Localizable.Str("Ошибка при чтении") + $" {testData[indexPath.Row].FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                                alert2.SetValueForKey(atr, new NSString("attributedTitle"));
                                alert2.AddAction(UIAlertAction.Create(Localizable.Str("Ок :("), UIAlertActionStyle.Cancel, null));
                                controller.PresentViewController(alert2, true, null);
                            }
                        });


                    examAlert.AddAction(cancelAction);
                    examAlert.AddAction(okayAction);

                    controller.PresentViewController(examAlert, true, null);

                }));
                #region Exam Region
                //alert.AddAction(UIAlertAction.Create(Localizable.Str("Экзамен"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                //{
                //    var examAlert = UIAlertController.Create(Localizable.Str("Экзамен"), null, UIAlertControllerStyle.Alert);


                //    UILabel numberOfQuestionsLabel = new UILabel();
                //    UILabel timerLabel = new UILabel();
                //    numberOfQuestionsLabel.Lines = 0;
                //    numberOfQuestionsLabel.Text = "Количество \nвопросов";
                //    timerLabel.Text = "Таймер(мин)";

                //    examAlert.View.AddSubview(numberOfQuestionsLabel);
                //    examAlert.View.AddSubview(timerLabel);

                //    examAlert.View.TranslatesAutoresizingMaskIntoConstraints = false;

                //    examAlert.View.HeightAnchor.ConstraintEqualTo(260).Active = true;

                //    //examAlert.View.TranslatesAutoresizingMaskIntoConstraints = true;

                //    //examAlert.View.HeightAnchor.ConstraintEqualTo(220).Active = true;

                //    numberOfQuestionsLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                //    numberOfQuestionsLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;
                //    numberOfQuestionsLabel.TopAnchor.ConstraintEqualTo(examAlert.View.TopAnchor, 50).Active = true;


                //    timerLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                //    timerLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;
                //    timerLabel.TopAnchor.ConstraintEqualTo(numberOfQuestionsLabel.BottomAnchor, 5).Active = true;
                //    //timerLabel.BottomAnchor.ConstraintEqualTo(examAlert.View.BottomAnchor, -35).Active = true;

                //    UITextField numberOfQuestions = new UITextField();
                //    UITextField timer = new UITextField();


                //    numberOfQuestions.ShouldChangeCharacters = (textField, range, replacement) =>
                //    {
                //        var newContent = new NSString(textField.Text).Replace(range, new NSString(replacement)).ToString();
                //        int? parsed;
                //        int number;

                //        bool isBiggerAndNum = false;
                //        try
                //        {
                //            if (replacement.Length != 0)
                //            {
                //                parsed = int.Parse(newContent);
                //                isBiggerAndNum = parsed == null ? false : parsed <= testData[indexPath.Row].NumberOfQuastions;
                //            }
                //        }
                //        catch
                //        { }
                //        return  isBiggerAndNum || replacement.Length == 0;
                //    };

                //    timer.ShouldChangeCharacters = (textField, range, replacement) =>
                //    {
                //        var newContent = new NSString(textField.Text).Replace(range, new NSString(replacement)).ToString();
                //        int number;
                //        return newContent.Length <= 3 && (replacement.Length == 0 || int.TryParse(replacement, out number));
                //    };

                //    numberOfQuestions.Placeholder = "Все";
                //    timer.Placeholder = "Нет";

                //    numberOfQuestions.TintColor = UIColor.Black;
                //    numberOfQuestions.BorderStyle = UITextBorderStyle.RoundedRect;

                //    timer.TintColor = UIColor.Black;
                //    timer.BorderStyle = UITextBorderStyle.RoundedRect;


                //    examAlert.View.AddSubview(numberOfQuestions);
                //    examAlert.View.AddSubview(timer);

                //    numberOfQuestions.TranslatesAutoresizingMaskIntoConstraints = false;

                //    numberOfQuestions.HeightAnchor.ConstraintEqualTo(30).Active = true;
                //    numberOfQuestions.WidthAnchor.ConstraintEqualTo(75).Active = true;
                //    numberOfQuestions.TopAnchor.ConstraintEqualTo(examAlert.View.TopAnchor, 50).Active = true;
                //    numberOfQuestions.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                //    timer.TranslatesAutoresizingMaskIntoConstraints = false;

                //    timer.HeightAnchor.ConstraintEqualTo(30).Active = true;
                //    timer.WidthAnchor.ConstraintEqualTo(75).Active = true;
                //    timer.TopAnchor.ConstraintEqualTo(numberOfQuestions.BottomAnchor, 10).Active = true;
                //    timer.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                //    UISwitch mixeQuestionsSwith = new UISwitch();
                //    UISwitch mixeAnsersSwith = new UISwitch();

                //    examAlert.View.AddSubview(mixeAnsersSwith);
                //    examAlert.View.AddSubview(mixeQuestionsSwith);

                //    mixeQuestionsSwith.TranslatesAutoresizingMaskIntoConstraints = false;

                //    mixeQuestionsSwith.TopAnchor.ConstraintEqualTo(timer.BottomAnchor, 10).Active = true;
                //    mixeQuestionsSwith.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;

                //    mixeAnsersSwith.TranslatesAutoresizingMaskIntoConstraints = false;

                //    mixeAnsersSwith.RightAnchor.ConstraintEqualTo(examAlert.View.RightAnchor, -15).Active = true;
                //    mixeAnsersSwith.TopAnchor.ConstraintEqualTo(mixeQuestionsSwith.BottomAnchor, 10).Active = true;

                //    UILabel mixeQuestionsLabel = new UILabel();
                //    UILabel mixeAnswersLabel = new UILabel();


                //    mixeQuestionsLabel.Text = "Перемешать вопросы";
                //    mixeAnswersLabel.Text = "Перемешать ответы";

                //    examAlert.View.AddSubview(mixeQuestionsLabel);
                //    examAlert.View.AddSubview(mixeAnswersLabel);

                //    mixeQuestionsLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                //    mixeQuestionsLabel.TopAnchor.ConstraintEqualTo(timerLabel.BottomAnchor, 17).Active = true;
                //    mixeQuestionsLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;


                //    mixeAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;

                //    mixeAnswersLabel.TopAnchor.ConstraintEqualTo(mixeQuestionsLabel.BottomAnchor, 20).Active = true;
                //    mixeAnswersLabel.LeftAnchor.ConstraintEqualTo(examAlert.View.LeftAnchor, 15).Active = true;


                //    var cancelAction = UIAlertAction.Create(Localizable.Str("Отмена"), UIAlertActionStyle.Cancel, null);
                //    var okayAction = UIAlertAction.Create(Localizable.Str("Ок"), UIAlertActionStyle.Default,
                //        (e) =>
                //        {
                //            try
                //            {
                //                int numOfQuestions = 0;
                //                int.TryParse(numberOfQuestions.Text, out numOfQuestions);

                //                bool MixeArr = (numOfQuestions == 0) || (numOfQuestions == testData[indexPath.Row].NumberOfQuastions) ? mixeQuestionsSwith.On : true;

                //                NewBetaViewController newBeta = new NewBetaViewController();
                //                newBeta.questionsList = testData[indexPath.Row].GetAllAnswers(null, MixeArr, mixeAnsersSwith.On).
                //                    Take(numOfQuestions == 0 ? testData[indexPath.Row].NumberOfQuastions : numOfQuestions).ToList();
                //                newBeta.testData = testData[indexPath.Row];
                //                long seconds = 0;
                //                long.TryParse(timer.Text, out seconds);
                //                newBeta.secondsForTimer = seconds * 60;
                //                controller.NavigationController.PushViewController(newBeta, true);
                //            }
                //            catch
                //            {
                //                var alert2 = UIAlertController.Create(Localizable.Str("Ошибка!"), null, UIAlertControllerStyle.Alert);
                //                var atr = new NSAttributedString(Localizable.Str("Ошибка при чтении") + $" {testData[indexPath.Row].FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                //                alert2.SetValueForKey(atr, new NSString("attributedTitle"));
                //                alert2.AddAction(UIAlertAction.Create(Localizable.Str("Ок :("), UIAlertActionStyle.Cancel, null));
                //                controller.PresentViewController(alert2, true, null);
                //            }
                //        });


                //    examAlert.AddAction(cancelAction);
                //    examAlert.AddAction(okayAction);

                //    controller.PresentViewController(examAlert, true, null);

                //}));
                #endregion

                alert.AddAction(UIAlertAction.Create(Localizable.Str("Править"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    try
                    {
                        ConstructorViewController constructor = new ConstructorViewController();
                        constructor.allCellsData = new AllCellsData(testData[indexPath.Row].GetAllAnswers( Numeration:false));
                        controller.NavigationController.PushViewController(constructor, true);
                    }
                    catch
                    {
                        var alert2 = UIAlertController.Create(Localizable.Str("Ошибка!"), null, UIAlertControllerStyle.Alert);
                        var atr = new NSAttributedString(Localizable.Str("Ошибка при чтении") + $" {testData[indexPath.Row].FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                        alert2.SetValueForKey(atr, new NSString("attributedTitle"));
                        alert2.AddAction(UIAlertAction.Create(Localizable.Str("Ок :("), UIAlertActionStyle.Cancel, null));
                        controller.PresentViewController(alert2, true, null);
                    }
                }));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Переименовать"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    var textInputAlertController = UIAlertController.Create(Localizable.Str("Переименовать тест?"), null, UIAlertControllerStyle.Alert);


                    textInputAlertController.AddTextField(textField =>
                    {
                        textField.Text = testData[indexPath.Row].RightFileName;
                    //textField.AddConstraint(NSLayoutConstraint.Create(textField, NSLayoutAttribute.Height, NSLayoutRelation.Equal, 1, 30));
                    textField.Font = UIFont.SystemFontOfSize(16);
                    });

                //Add Actions
                var cancelAction = UIAlertAction.Create(Localizable.Str("Отмена"), UIAlertActionStyle.Cancel, null);
                    var okayAction = UIAlertAction.Create(Localizable.Str("Ок"), UIAlertActionStyle.Default, alertAction =>
                    {

                        testData[indexPath.Row].RightFileName = textInputAlertController.TextFields[0].Text;
                        testData[indexPath.Row].RenameFile();
                        SerializeExtension.Serialize(testData);
#if RELEASE_ANIMATIONS
                    tableView.BeginUpdates();
                    tableView.ReloadRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Automatic);
                    tableView.EndUpdates();
#else
                    UIView.Transition(tableView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { tableView.ReloadData(); }, null);
#endif
                });
                    textInputAlertController.AddAction(cancelAction);
                    textInputAlertController.AddAction(okayAction);

                //Present Alert
                controller.PresentViewController(textInputAlertController, true, null);
                }));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Удалить"), UIAlertActionStyle.Default, (UIAlertAction obj) =>
                {
                    var fileDeleteAlert = UIAlertController.Create(Localizable.Str("Удалить?"), null, UIAlertControllerStyle.Alert);
                    var cancelAction = UIAlertAction.Create(Localizable.Str("Отмена"), UIAlertActionStyle.Cancel, null);
                    var okayAction = UIAlertAction.Create(Localizable.Str("Да"), UIAlertActionStyle.Destructive, (UIAlertAction obj2) =>
                    {

                        testData[indexPath.Row].RemoveFile();
                        testData.RemoveAt(indexPath.Row);
                        SerializeExtension.Serialize(testData);
#if RELEASE_ANIMATIONS
                    tableView.BeginUpdates();
                    tableView.DeleteRows(new NSIndexPath[] { indexPath }, UITableViewRowAnimation.Automatic);
                    tableView.EndUpdates();
#else
                    UIView.Transition(tableView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { tableView.ReloadData(); }, null);
#endif
                });

                    fileDeleteAlert.AddAction(cancelAction);
                    fileDeleteAlert.AddAction(okayAction);
                    controller.PresentViewController(fileDeleteAlert, true, null);

                }));
                controller.PresentViewController(alert, true, () =>
                {
                    alert.View.Superview.UserInteractionEnabled = true;
                    alert.View.Superview?.AddGestureRecognizer(new UITapGestureRecognizer(() =>
                    alert.DismissViewController(true, null)));

                });
                tableView.DeselectRow(indexPath, true);
            }

        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return testData.Count;
        }
    }
}