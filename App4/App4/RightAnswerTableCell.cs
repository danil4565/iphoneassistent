using Foundation;
using System;
using System.Collections.Generic;
using UIKit;


namespace App4
{
    public partial class RightAnswerTableCell : UITableViewCell
    {
        public RightAnswerTableCell (IntPtr handle) : base (handle)
        {
        }

        public void UpdateCell(List<string> list)
        {
            Quastion.Text = list[0];
            RightAnswer.Text = "";
            for (int i = 1; i < list.Count; i++)
                RightAnswer.Text += (list[i] + "\n");

        }
        
    }
}