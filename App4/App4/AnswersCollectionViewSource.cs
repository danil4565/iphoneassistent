﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Foundation;
using UIKit;

namespace App4
{
    class AnswersCollectionViewSource : UICollectionViewSource
    {
        AllCellsData AllCellsData;
        List<Questions> questionsList;
        bool MultAnsw;

        public AnswersCollectionViewSource(AllCellsData allCellsData, List<Questions> questionsList, bool MultAnswers)
        {
            MultAnsw = MultAnswers;
            this.questionsList = questionsList;
            AllCellsData = allCellsData;

        }

        public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
        {
            var cell = (AnswersCollectionCell)collectionView.DequeueReusableCell(AnswersCollectionCell.reuseIdentifier, indexPath);

            cell.UpdateCell(AllCellsData, questionsList[indexPath.Row], indexPath.Row, MultAnsw);

            return cell;
        }

        public override nint GetItemsCount(UICollectionView collectionView, nint section)
        {
            return questionsList.Count;
        }

        public override nint NumberOfSections(UICollectionView collectionView)
        {
            return 1;
        }
        public override void WillDisplayCell(UICollectionView collectionView, UICollectionViewCell cell, NSIndexPath indexPath)
        {
            var cell2 = (AnswersCollectionCell)cell;
            cell2.answersTable.ReloadData();
            cell2.answersTable.ReloadSections(NSIndexSet.FromIndex(0), UITableViewRowAnimation.Fade);
        }

    }
}