﻿using System;
using CoreGraphics;
using Foundation;
using UIKit;

namespace App4
{
    internal class ConstructorQuastionsListCell : UICollectionViewCell
    {
        public static string reuseIdentifier { get; } = "constructorQuastionsListCell";


        UILabel number;

        public ConstructorQuastionsListCell(IntPtr handle) : base(handle)
        {
            Layer.BorderColor = UIColor.Black.CGColor;
            Layer.BorderWidth = (nfloat)0.5;
            Layer.CornerRadius = this.Frame.Width / 2;
            Layer.MasksToBounds = true;
            number = new UILabel(new CGRect(0, 0, Bounds.Width, Bounds.Height));
            number.BackgroundColor = UIColor.LightTextColor;
            number.TextAlignment = UITextAlignment.Center;
            AddSubview(number);
            BackgroundColor = UIColor.GroupTableViewBackgroundColor;
        }

        public void UpdateCell(string index, UIColor color)
        {
            BackgroundColor = color == null ? UIColor.GroupTableViewBackgroundColor : color;
            if (index == "+")
            {
                number.Text = index;

                number.Font = UIFont.SystemFontOfSize(20);
                number.TextColor = UIColor.FromRGB(0, 122, 255);
            }
            else
            {
                number.Font = UIFont.SystemFontOfSize(16);

                number.Text = index;
                number.TextColor = UIColor.Black;
            }
        }


    }
}