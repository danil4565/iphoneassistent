// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("QuastionsListCell")]
    partial class QuastionsListCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel NumberOfQuastion { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (NumberOfQuastion != null) {
                NumberOfQuastion.Dispose ();
                NumberOfQuastion = null;
            }
        }
    }
}