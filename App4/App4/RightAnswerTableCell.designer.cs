// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace App4
{
    [Register ("RightAnswerTableCell")]
    partial class RightAnswerTableCell
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Quastion { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel RightAnswer { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Quastion != null) {
                Quastion.Dispose ();
                Quastion = null;
            }

            if (RightAnswer != null) {
                RightAnswer.Dispose ();
                RightAnswer = null;
            }
        }
    }
}