﻿using CoreGraphics;
using Foundation;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Essentials;

namespace App4
{
    public class ConstructorViewController : UIViewController
    {
        
        public enum FileExtention { txt, qst, it2, qzt }
        UIButton DeleteButton, NextButton, PreviewButton;
        NSTimer loopTimer;
        Int selectedIndex;
        int itemsCountMem;
        QuestionsDesignerData Designer;
        public AllCellsData allCellsData = new AllCellsData();
        UICollectionView QuestionListCollection, AnswersCollectionView;
        public override void ViewDidLoad()
        {


            var button = new UIButton(UIButtonType.Custom);
            button.Frame = new CGRect(0, 0, 24, 24);
            button.SetImage(UIImage.FromBundle("icons8-save-100.png"), UIControlState.Normal);
            var barItem = new UIBarButtonItem(button);
            barItem.CustomView.TranslatesAutoresizingMaskIntoConstraints = false;
            barItem.CustomView.HeightAnchor.ConstraintEqualTo(24).Active = true;
            barItem.CustomView.WidthAnchor.ConstraintEqualTo(24).Active = true;

            button.TouchDown += (o, e) =>
            {

                UIView.Animate(0.15, () =>
                {
                    button.Transform = CGAffineTransform.MakeScale(0.995f, 0.995f);
                    button.Alpha = 0.5f;
                });
            };
            button.TouchUpInside += async (o, e) =>
            {
               Designer = new QuestionsDesignerData(allCellsData);


                var extentionAlert = UIAlertController.Create("", "", UIAlertControllerStyle.Alert);
                UILabel enterFileNameLabel = new UILabel();
                enterFileNameLabel.Text = "Введите имя";

                extentionAlert.View.AddSubview(enterFileNameLabel);

                enterFileNameLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                enterFileNameLabel.TopAnchor.ConstraintEqualTo(extentionAlert.View.TopAnchor, 15).Active = true;
                enterFileNameLabel.LeftAnchor.ConstraintEqualTo(extentionAlert.View.LeftAnchor, 15).Active = true;

                UITextField fileNameField = new UITextField();

                extentionAlert.View.AddSubview(fileNameField);

                fileNameField.TranslatesAutoresizingMaskIntoConstraints = false;
                fileNameField.TopAnchor.ConstraintEqualTo(extentionAlert.View.TopAnchor, 15).Active = true;
                fileNameField.LeftAnchor.ConstraintEqualTo(enterFileNameLabel.RightAnchor, 15).Active = true;
                fileNameField.RightAnchor.ConstraintEqualTo(extentionAlert.View.RightAnchor,  -15).Active = true;

                UITableView extentionsTabel = new UITableView();
                extentionsTabel.RegisterClassForCellReuse(typeof(ExtentionTableCell), ExtentionTableCell.ReuseIndentifier);
                string[] extenions = { "txt", "qst", "it2", "qsz" };
                extentionsTabel.Source = new ExtentionTableSuorce(extenions);
               //Designer.SaveTestIt2("Тест в формате it2");
               //Designer.SaveTestQst("Тест в формате qst");
               //Designer.SaveTestTxt("Тест в формате txt");
                if (Designer.Check)
                {

                    // тут должен быть код вьюхи с выбором имени и формата

                    Designer.SaveTestQsz("Тест в формате qsz");
                    if (Designer.FilePathSaveTest != null)
                    {
                        await Share.RequestAsync(new ShareFileRequest
                        {
                            File = new ShareFile(Designer.FilePathSaveTest)
                        });
                        //File.Delete(Designer.FilePathSaveTest);
                    }
                }
               UIView.Animate(0.15, () =>
               {
                   button.Transform = CGAffineTransform.MakeIdentity();
                   button.Alpha = 1f;
               });
           };
            button.TouchDragExit += (o, e) =>
            {
                UIView.Animate(0.15, () =>
                {
                    button.Transform = CGAffineTransform.MakeIdentity();
                    button.Alpha = 1f;
                });
            };

            NavigationItem.RightBarButtonItem = barItem;

            selectedIndex = new Int() { intIndex = 0 };

            QuestionListCollection = new UICollectionView(new CGRect(0, 0, 100, 100), new UICollectionViewLayout());

            View.AddSubview(QuestionListCollection);

            QuestionListCollection.TranslatesAutoresizingMaskIntoConstraints = false;

            QuestionListCollection.TopAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.TopAnchor, 5).Active = true;
            QuestionListCollection.LeftAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.LeftAnchor, 5).Active = true;
            QuestionListCollection.RightAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.RightAnchor, -5).Active = true;
            QuestionListCollection.HeightAnchor.ConstraintEqualTo(48).Active = true;

            QuestionListCollection.RegisterClassForCell(typeof(ConstructorQuastionsListCell), ConstructorQuastionsListCell.reuseIdentifier);

            AnswersCollectionView = new UICollectionView(new CGRect(0, 0, 100, 100), new UICollectionViewLayout());
            View.AddSubview(AnswersCollectionView);
            AnswersCollectionView.TranslatesAutoresizingMaskIntoConstraints = false;

            AnswersCollectionView.TopAnchor.ConstraintEqualTo(QuestionListCollection.BottomAnchor, 10).Active = true;
            AnswersCollectionView.LeftAnchor.ConstraintEqualTo(View.LeftAnchor).Active = true;
            AnswersCollectionView.RightAnchor.ConstraintEqualTo(View.RightAnchor).Active = true;

            AnswersCollectionView.RegisterClassForCell(typeof(ConstructorAnswersCollectionCell), ConstructorAnswersCollectionCell.reuseIdentifier);
            AnswersCollectionView.RegisterClassForCell(typeof(ConstructorAddAnswersCollectionCell), ConstructorAddAnswersCollectionCell.reuseIdentifier);


            AnswersCollectionView.ReloadData();


            QuestionListCollection.Source = new ConstructorQuestionListCollectionSource(AnswersCollectionView, allCellsData, selectedIndex);

            AnswersCollectionView.Source = new ConstructorAnswersCollectionViewSource(QuestionListCollection, allCellsData);

            DeleteButton = new UIButton(UIButtonType.System);
            DeleteButton.SetTitle(Localizable.Str("Удалить"), UIControlState.Normal);
            DeleteButton.Font = UIFont.SystemFontOfSize(16);
            DeleteButton.TintColor = UIColor.Red;

            DeleteButton.TouchUpInside +=
            (o, e) =>
            {
                int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;
                if (cellIndex != allCellsData.questionCells.Count)
                {

                    allCellsData.questionCells.RemoveAt(cellIndex);
                    AnswersCollectionView.DeleteItems(new NSIndexPath[] { AnswersCollectionView.IndexPathsForVisibleItems[0] });
                    QuestionListCollection.DeleteItems(new NSIndexPath[] { AnswersCollectionView.IndexPathsForVisibleItems[0] });
                }

                UIView.Transition(QuestionListCollection, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { QuestionListCollection.ReloadData(); }, null);
                QuestionListCollection.ReloadSections(NSIndexSet.FromIndex(0));

            };



            View.AddSubview(DeleteButton);
            DeleteButton.TranslatesAutoresizingMaskIntoConstraints = false;
            DeleteButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
            DeleteButton.TopAnchor.ConstraintEqualTo(AnswersCollectionView.BottomAnchor, 5).Active = true;
            DeleteButton.CenterXAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.CenterXAnchor).Active = true;
            DeleteButton.WidthAnchor.ConstraintEqualTo(90).Active = true;
            DeleteButton.UserInteractionEnabled = true;
            DeleteButton.Enabled = true;

            NextButton = new UIButton(UIButtonType.System);
            PreviewButton = new UIButton(UIButtonType.System);

            NextButton.SetTitle(Localizable.Str("Далее"), UIControlState.Normal);
            PreviewButton.SetTitle(Localizable.Str("Назад"), UIControlState.Normal);

            View.AddSubview(NextButton);
            View.AddSubview(PreviewButton);

            NextButton.TranslatesAutoresizingMaskIntoConstraints = false;

            NextButton.LeftAnchor.ConstraintEqualTo(DeleteButton.RightAnchor, 30).Active = true;
            NextButton.RightAnchor.ConstraintEqualTo(View.RightAnchor, -30).Active = true;
            NextButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
            NextButton.HeightAnchor.ConstraintEqualTo(DeleteButton.HeightAnchor).Active = true;

            //NextButton.TitleEdgeInsets = new UIEdgeInsets(top: -12, left: -50, bottom: -12, right: -16);


            PreviewButton.TranslatesAutoresizingMaskIntoConstraints = false;

            PreviewButton.LeftAnchor.ConstraintEqualTo(View.LeftAnchor, 30).Active = true;
            PreviewButton.RightAnchor.ConstraintEqualTo(DeleteButton.LeftAnchor, -30).Active = true;
            PreviewButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
            PreviewButton.HeightAnchor.ConstraintEqualTo(DeleteButton.HeightAnchor).Active = true;

            NextButton.TouchUpInside +=
            (o, e) =>
            {
                int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;
                if (cellIndex != allCellsData.questionCells.Count)
                    AnswersCollectionView.ScrollToItem(NSIndexPath.FromItemSection(cellIndex + 1, 0), UICollectionViewScrollPosition.CenteredHorizontally, false);
            };
            PreviewButton.TouchUpInside +=
            (o, e) =>
            {
                int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;
                if (cellIndex != 0)
                    AnswersCollectionView.ScrollToItem(NSIndexPath.FromItemSection(cellIndex - 1, 0), UICollectionViewScrollPosition.CenteredHorizontally, false);
            };

            loopTimer = NSTimer.CreateRepeatingTimer(0.3, (t) =>
            {
                PreviewButton.Enabled = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row != 0;

                NextButton.Enabled = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row !=
                    allCellsData.questionCells.Count;

                DeleteButton.Enabled = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row !=
                    allCellsData.questionCells.Count;

                int index = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1) == null ?
                    0 : AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row;
                if (index != selectedIndex.intIndex)
                {
                    var old = selectedIndex.intIndex;
                    selectedIndex.intIndex = index;
                    QuestionListCollection.ReloadItems(new NSIndexPath[] { NSIndexPath.FromItemSection(old, 0), NSIndexPath.FromItemSection(index, 0),
                         });
                }
                if (itemsCountMem != allCellsData.questionCells.Count && (selectedIndex.intIndex == (allCellsData.questionCells.Count - 1) || allCellsData.questionCells.Count == 0))
                {
                    InvokeOnMainThread(async () => { await Task.Delay(250); QuestionListCollection.ReloadItems(new NSIndexPath[] { NSIndexPath.FromItemSection(allCellsData.questionCells.Count, 0) }); });

                    itemsCountMem = allCellsData.questionCells.Count;
                }

            });
            NSRunLoop.Main.AddTimer(loopTimer, NSRunLoopMode.Common);

            base.ViewDidLoad();
            itemsCountMem = allCellsData.questionCells.Count;

            QuestionListCollection.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;
            AnswersCollectionView.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;

            View.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;

        }
        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            var cellSize = new CGSize(QuestionListCollection.Bounds.Height - 5, QuestionListCollection.Bounds.Height - 5);
            //QuestionListCollection.PagingEnabled = true;
            var layout = new UICollectionViewFlowLayout();
            layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            layout.ItemSize = cellSize;
            QuestionListCollection.SetCollectionViewLayout(layout, animated: true);

            var cellSize2 = new CGSize(AnswersCollectionView.Bounds.Width, AnswersCollectionView.Bounds.Height);
            AnswersCollectionView.PagingEnabled = true;
            var layout2 = new UICollectionViewFlowLayout();
            layout2.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
            layout2.ItemSize = cellSize2;
            layout2.MinimumLineSpacing = 0;
            layout2.MinimumInteritemSpacing = 0;
            AnswersCollectionView.SetCollectionViewLayout(layout2, animated: true);

        }
        public override void ViewDidDisappear(bool animated)
        {
            if (IsMovingFromParentViewController)
            {
                if (Designer != null)
                {
                    Directory.Delete(Designer.Cache,true);
                }
                loopTimer.Invalidate();
                loopTimer.Dispose();
                loopTimer = null;
            }
        }

    }
    class Int
    {
        public int intIndex;
    }
}
