﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace App4
{
    class ConstructorAddAnswersCollectionCell : UICollectionViewCell
    {
        public static string reuseIdentifier { get; } = "constructorAddAnswersCollectionCell";

        private UICollectionView questionsCollection;
        private AllCellsData cellsData;
        private NSIndexPath index;
        UICollectionView collectioin;

        protected internal ConstructorAddAnswersCollectionCell(IntPtr handle) : base(handle)
        {
            UIButton addButton = new UIButton();
            addButton.SetTitle("+", UIControlState.Normal);
            addButton.Font = UIFont.SystemFontOfSize(30);
            addButton.SetTitleColor(UIColor.FromRGB(0, 122, 255), UIControlState.Normal);
            AddSubview(addButton);

            addButton.TranslatesAutoresizingMaskIntoConstraints = false;

            addButton.Layer.BorderColor = UIColor.FromRGB(0, 122, 255).CGColor;
            addButton.Layer.CornerRadius = 5;
            addButton.Layer.BorderWidth = (nfloat)1;

            addButton.TopAnchor.ConstraintEqualTo(TopAnchor, 10).Active = true;
            addButton.LeftAnchor.ConstraintEqualTo(LeftAnchor, 15).Active = true;
            addButton.RightAnchor.ConstraintEqualTo(RightAnchor, -15).Active = true;
            addButton.BottomAnchor.ConstraintEqualTo(BottomAnchor, -10).Active = true;
            addButton.HeightAnchor.ConstraintEqualTo(35).Active = true;



            addButton.TouchDown += (e, a) =>
            {
                UIView.Animate(0.20, () =>
                {
                    addButton.Transform = CGAffineTransform.MakeScale(0.99f, 0.99f);
                    addButton.Alpha = 0.6f;
                });
            };

            addButton.TouchUpInside += (e, a) =>
            {
                cellsData.questionCells.Add(new QuestionCellData(1));
                collectioin.InsertItems(new NSIndexPath[] { index });
                questionsCollection.InsertItems(new NSIndexPath[] { index });
                collectioin.ScrollToItem(NSIndexPath.FromItemSection(index.Row, 0), UICollectionViewScrollPosition.Left, true);

                UIView.Animate(0.23, () =>
                {
                    addButton.Transform = CGAffineTransform.MakeIdentity();
                    addButton.Alpha = 1f;
                });
            };

            addButton.TouchDragExit += (e, a) =>
            {
                UIView.Animate(0.15, () =>
                {
                    addButton.Transform = CGAffineTransform.MakeIdentity();
                    addButton.Alpha = 1f;
                });
            };
        }



        public void UpdateCell(UICollectionView collectionView, NSIndexPath indexPath, AllCellsData cellsData, UICollectionView questionsCollection)
        {
            this.questionsCollection = questionsCollection;
            this.cellsData = cellsData;
            index = indexPath;
            collectioin = collectionView;
        }
    }
}