using Foundation;
using Plugin.FilePicker;
using Plugin.FilePicker.Abstractions;
using System;
using System.Collections.Generic;
using UIKit;

namespace App4
{
    public partial class TestControll : UITableViewController
    {
        public TestControll(IntPtr handle) : base(handle)
        {
            tableItems = new List<TestData>();
        }

        //������ ���� ������

        public List<TestData> tableItems;


        //private void Serial()
        //{
        //    Documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //    Stream SaveFileStream = File.Create(Path.Combine(Documents, "SaveLoad.bin"));
        //    BinaryFormatter serializer = new BinaryFormatter();
        //    serializer.Serialize(SaveFileStream, tableItems);
        //    SaveFileStream.Close();
        //}
        public override void ViewDidLoad()
        {
            MainNavigItem.Title = "������ ������";
            tableItems = SerializeExtension.Deserialize() ?? new List<TestData>();
            if (tableItems.Count != 0)
            {
                MainTable.Source = new MainTableViewSource(tableItems, this);
            }
            MainTable.RowHeight = UITableView.AutomaticDimension;
            MainTable.EstimatedRowHeight = 40f;
            MainTable.ReloadData();

            base.ViewDidLoad();


        }



        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private bool IsDataValid(FileData fileData)
        {

            for (int i = 0; i < tableItems.Count; i++)
            {
                if (tableItems[i].FileName == fileData.FileName)
                {
                    return false;
                }
            }
            return true;

        }

        //������ ������������ � ������ ����� 


        async partial void UIBarButtonItem28213_Activated(UIBarButtonItem sender)
        {

            try
            {
                var fileData = await CrossFilePicker.Current.PickFile();
                bool isDataValid = IsDataValid(fileData);
                if (fileData != null && isDataValid)
                {

                    tableItems.Add(new TestData(fileData));
                    SerializeExtension.Serialize(tableItems);
                    if (tableItems.Count != 0)
                    {
                        MainTable.Source = new MainTableViewSource(tableItems, this);
                    }
                    MainTable.RowHeight = UITableView.AutomaticDimension;
                    MainTable.EstimatedRowHeight = 40f;
                    MainTable.ReloadData();
                }
                if (!isDataValid)
                {
                    //Send Eror
                }
                else
                {

                }
            }
            catch { }
        }
    }
}