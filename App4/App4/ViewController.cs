﻿//#define RELEASE_ANIMATIONS
using CoreGraphics;
using CoreText;
using Plugin.FilePicker;
using System;
using System.Globalization;
using System.Collections.Generic;
using System.IO;
using UIKit;
//using System.Runtime.Serialization.Formatters.Binary;
//using System.Runtime.Serialization;

//using Newtonsoft.Json;


namespace App4
{

    public partial class ViewController : UIViewController
    {

        public List<TestData> tableItems;
        AppDelegate MyDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;

        public ViewController(IntPtr handle) : base(handle)
        {
            MyDelegate.Controller = this;
            tableItems = new List<TestData>();
        }



        public override void ViewDidLoad()
        {
            MainNavigItem.Title = Localizable.Str("Список тестов");
            tableItems = SerializeExtension.Deserialize() ?? new List<TestData>();
            if (tableItems.Count != 0)
            {
                MainTableView.Source = new MainTableViewSource(tableItems, this);
            }
            MainTableView.RowHeight = UITableView.AutomaticDimension;
            MainTableView.EstimatedRowHeight = 40f;
            MainTableView.ReloadData();
            MainTableView.TableFooterView = new UIView();
            //MainTableView.AlwaysBounceVertical = false;
            base.ViewDidLoad();
            bool f = IsDark.Dark;
        }





        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        private bool IsDataValid(string FileName)
        {

            for (int i = 0; i < tableItems.Count; i++)
            {
                if (tableItems[i].FileName == FileName)
                {
                    return false;
                }
            }
            return true;
        }
        private bool IsDataValidFileType(string FileName)
        {
            if(FileName.Contains(".qst")|| FileName.Contains(".qsz")|| FileName.Contains(".it2")|| FileName.Contains(".txt"))
            {
                return true;
            }
            else { return false; }
        }




        public void AddTestFile(string Path)
        {

            try
            {
                var FileInfo = new FileInfo(Path);
                bool isDataValid = IsDataValid(FileInfo.Name);
                bool isDataValidFileType = IsDataValidFileType(FileInfo.Name);

                if (FileInfo != null && isDataValid && isDataValidFileType)
                {
                    tableItems.Add(new TestData(FileInfo.Name, FileInfo.FullName));
                    SerializeExtension.Serialize(tableItems);
                    if (tableItems.Count != 0)
                    {
                        MainTableView.Source = new MainTableViewSource(tableItems, this);
                    }
                    MainTableView.RowHeight = UITableView.AutomaticDimension;
                    MainTableView.EstimatedRowHeight = 40f;


#if RELEASE_ANIMATIONS
                    MainTableView.BeginUpdates();
                    MainTableView.InsertRows(new NSIndexPath[] { NSIndexPath.FromRowSection(tableItems.Count - 1, 0) },
                        UITableViewRowAnimation.Automatic);
                    MainTableView.EndUpdates();
#else
                    UIView.Transition(MainTableView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { MainTableView.ReloadData(); }, null);
#endif

                }
                if (!isDataValid)
                {
                    Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 1.5, 0.3, Localizable.Str("Файл уже существует !!!"));
                }
                if (!isDataValidFileType)
                {
                    Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 1.5, 0.3, Localizable.Str("Файл c данным расширением не поддерживается !!!"));
                }
               
            }
            catch { };

        }

        async partial void AddTestButton_Activated(UIBarButtonItem sender)
        {
            try
            {
                var fileData = await CrossFilePicker.Current.PickFile();
                bool isDataValidFileType = IsDataValidFileType(fileData.FileName);
                bool isDataValid = IsDataValid(fileData.FileName);
                if (fileData != null && isDataValid && isDataValidFileType)
                {

                    tableItems.Add(new TestData(fileData.FileName, fileData.FilePath));
                    SerializeExtension.Serialize(tableItems);
                    if (tableItems.Count != 0)
                    {
                        MainTableView.Source = new MainTableViewSource(tableItems, this);
                    }
                    MainTableView.RowHeight = UITableView.AutomaticDimension;
                    MainTableView.EstimatedRowHeight = 40f;
#if RELEASE_ANIMATIONS
                    MainTableView.BeginUpdates();
                    MainTableView.InsertRows(new NSIndexPath[] { NSIndexPath.FromRowSection(tableItems.Count - 1, 0) },
                        UITableViewRowAnimation.Automatic);
                    MainTableView.EndUpdates();
#else
                    UIView.Transition(MainTableView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { MainTableView.ReloadData(); }, null);

#endif
                }
                if (!isDataValid)
                {
                    Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 1.5, 0.3, Localizable.Str("Файл уже существует !!!"));
                }
                if (!isDataValidFileType)
                {
                    Toast.showAlert(new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 0.90f), UIColor.White, 1.5, 0.3, Localizable.Str("Файл c данным расширением не поддерживается !!!"));
                }
            }
            catch { }
        }

        partial void CreateTestButton_Activated(UIBarButtonItem sender)
        {
            ConstructorViewController constructor = new ConstructorViewController();
            this.NavigationController.PushViewController(constructor, true);

        }


    }
    public class Toast
    {
        public static void showAlert(UIColor backgroundColor, UIColor textColor, double duration, double delay, string message)
        {
            var appDelegate = UIApplication.SharedApplication.Delegate as AppDelegate;
            var label = new UILabel(frame: CGRect.Empty);
            label.TextAlignment = UITextAlignment.Center;
            label.Text = message;
            label.Font = UIFont.SystemFontOfSize(20);
            label.AdjustsFontSizeToFitWidth = true;

            label.BackgroundColor = backgroundColor;
            label.TextColor = textColor;

            //label.Layer.CornerRadius = 5;
            //label.Layer.MasksToBounds = true;

            label.SizeToFit();
            label.Lines = 4;
            label.Layer.ShadowColor = UIColor.Gray.CGColor;
            label.Layer.ShadowOffset = new CGSize(width: 4, height: 3);
            label.Layer.ShadowOpacity = 0.3f;
            label.Frame = new CGRect(x: 0, y: appDelegate.Window.Frame.Size.Height, width: appDelegate.Window.Frame.Size.Width, height: 44);

            label.Alpha = 1;

            appDelegate.Window.AddSubview(label);

            var basketTopFrame = label.Frame;
            basketTopFrame.Y -= 44;

            UIView.AnimateNotify(duration, delay, 0.5f, 0.1f, UIViewAnimationOptions.CurveEaseOut, () => { label.Frame = basketTopFrame; }, (finished) =>
            {
                UIView.AnimateNotify(duration, delay, 0.5f, 0.1f, UIViewAnimationOptions.CurveEaseIn, () =>
                {
                    basketTopFrame.Y += 44;
                    label.Frame = basketTopFrame;
                },
                (f) =>
                {
                    label.RemoveFromSuperview();
                });


            });


        }

    }

    static class IsDark
    {
        public static bool Dark {
            get
            {
                if (int.Parse(UIDevice.CurrentDevice.SystemVersion.Split(',', '.')[0]) >= 13)
                {
                    //return true;
                    return UITraitCollection.CurrentTraitCollection.UserInterfaceStyle == UIUserInterfaceStyle.Dark;
                }
                return false;
            } }
    }

}
