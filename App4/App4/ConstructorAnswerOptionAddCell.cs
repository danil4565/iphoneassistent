﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreGraphics;
using Foundation;
using UIKit;

namespace App4
{
    class ConstructorAnswerOptionAddCell : UITableViewCell
    {
        public static string reuseIdentifier { get; } = "constructorAnswerOptionAddCell";

        private AllCellsData cellsData;
        private NSIndexPath index;
        private UITableView table;
        private int itemIndex;

        protected internal ConstructorAnswerOptionAddCell(IntPtr handle) : base(handle)
        {
            UIButton addButton = new UIButton();
            addButton.SetTitle("+", UIControlState.Normal);
            addButton.Font = UIFont.SystemFontOfSize(30);
            addButton.SetTitleColor(UIColor.FromRGB(0, 122, 255), UIControlState.Normal);
            AddSubview(addButton);

            addButton.TranslatesAutoresizingMaskIntoConstraints = false;

            addButton.Layer.BorderColor = UIColor.FromRGB(0, 122, 255).CGColor;
            addButton.Layer.CornerRadius = 5;
            addButton.Layer.BorderWidth = (nfloat)1;

            addButton.TopAnchor.ConstraintEqualTo(TopAnchor, 10).Active = true;
            addButton.LeftAnchor.ConstraintEqualTo(LeftAnchor, 15).Active = true;
            addButton.RightAnchor.ConstraintEqualTo(RightAnchor, -15).Active = true;
            addButton.BottomAnchor.ConstraintEqualTo(BottomAnchor, -10).Active = true;
            addButton.HeightAnchor.ConstraintEqualTo(35).Active = true;

            addButton.TouchDown += (e, a) =>
            {
                UIView.Animate(0.20, () =>
                {
                    addButton.Transform = CGAffineTransform.MakeScale(0.99f, 0.99f);
                    addButton.Alpha = 0.6f;
                });
            };

            addButton.TouchUpInside += (e, a)=>
            {
                table.BeginUpdates();
                cellsData[itemIndex].answers.Add(new AnswerCellData());
                var row = NSIndexPath.FromRowSection(cellsData[itemIndex].answers.Count - 1, 0);
                table.InsertRows(new NSIndexPath[] { row }, UITableViewRowAnimation.Top);
                table.EndUpdates();
                table.LayoutIfNeeded();
                table.ScrollToRow(NSIndexPath.FromRowSection(cellsData[itemIndex].answers.Count, 0), UITableViewScrollPosition.Bottom, true);
                UIView.Animate(0.23, () =>
                {
                        addButton.Transform = CGAffineTransform.MakeIdentity();
                        addButton.Alpha = 1f;
                });
            };

            addButton.TouchDragExit += (e, a) =>
            {
                UIView.Animate(0.15, () =>
                {
                    addButton.Transform = CGAffineTransform.MakeIdentity();
                    addButton.Alpha = 1f;
                });
            };

        }

        public void UpdateCell(UITableView tableView, NSIndexPath indexPath, AllCellsData cellsData, int itemIndex)
        {
            this.itemIndex = itemIndex;
            this.cellsData = cellsData;
            index = indexPath;
            table = tableView;
        }

    }
}