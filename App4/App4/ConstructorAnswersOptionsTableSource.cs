﻿using System;
using Foundation;
using UIKit;

namespace App4
{
    internal class ConstructorAnswersOptionsTableSource : UITableViewSource
    {

        AllCellsData cellsData;
        int itemIndex;

        public ConstructorAnswersOptionsTableSource(AllCellsData cellsData, int itemIndex)
        {
            this.itemIndex = itemIndex;
            this.cellsData = cellsData;
        }

        public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
        {
            if (indexPath.Row == cellsData[itemIndex].answers.Count)
            {
                var cell = (ConstructorAnswerOptionAddCell)tableView.DequeueReusableCell(ConstructorAnswerOptionAddCell.reuseIdentifier);
                cell.UpdateCell(tableView, indexPath, cellsData, itemIndex);
                cell.SelectionStyle = UITableViewCellSelectionStyle.None;

                return cell;
            }
            else 
            {
                var cell = (ConstructorAnswerOptionCell)tableView.DequeueReusableCell(ConstructorAnswerOptionCell.reuseIdentifier, indexPath);
                cell.UpdateCell(tableView, indexPath, cellsData, itemIndex);
                cell.SelectionStyle = UITableViewCellSelectionStyle.None;

                return cell;
            }
        }

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            return cellsData[itemIndex].answers.Count + 1;
        }
        public override nfloat GetHeightForRow(UITableView tableView, NSIndexPath indexPath)
        {
            return UITableView.AutomaticDimension;
        }
    }
}