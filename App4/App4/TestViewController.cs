﻿#define DEBUG
using CoreGraphics;
using Foundation;
using System.Collections.Generic;
using System.Linq;
using UIKit;

namespace App4
{
    internal class TestViewController : UIViewController
    {
        public long secondsForTimer = 0;

        public TestData testData;

        public List<Questions> questionsList;

        AllCellsData testDataOfCells;

        UICollectionView QuestionListCollection, AnswersCollectionView;

        UIColor green, red, yellow;

        List<UIColor> colorsForQuestionsList = new List<UIColor>();

        bool IsTimerOn = false;

        UIButton ApplyButton, NextButton, PreviewButton;

        NSTimer timer, loopTimer;

        int rightAnswers = 0;
        int questionsAnswered = 0;
        int errors = 0;

        public override void ViewDidLoad()
        {

            green = new UIColor(red: 0.58f, green: 0.98f, blue: 0.52f, alpha: 1.0f);
            red = new UIColor(red: 1.00f, green: 0.41f, blue: 0.38f, alpha: 1.0f);
            yellow = new UIColor(red: 1.00f, green: 1.00f, blue: 0.40f, alpha: 1.0f);



            var timerLabel = new UILabel();
            timerLabel.Text = "";
            timerLabel.Font = UIFont.BoldSystemFontOfSize(16);

            timerLabel.TranslatesAutoresizingMaskIntoConstraints = false;

            timerLabel.WidthAnchor.ConstraintEqualTo(75).Active = true;

            timer = NSTimer.CreateRepeatingScheduledTimer(1, (timerSender) =>
            {
                if (IsTimerOn)
                {
                    if (secondsForTimer > -1)
                    {
                        timerLabel.Text = string.Format($"{(int)secondsForTimer / 3600:D2}:{(int)secondsForTimer / 60 % 60:D2}:{(int)secondsForTimer % 60:D2}");
                        secondsForTimer--;
                    }
                    else
                    {
                        timerSender.Invalidate();
                        timerSender.Dispose();
                        timerSender = null;

                        var timerEndAlert = UIAlertController.Create(Localizable.Str("Время вышло!"), null, UIAlertControllerStyle.Alert);
                        timerEndAlert.AddAction(UIAlertAction.Create(Localizable.Str("Ок"), UIAlertActionStyle.Cancel, (UIAlertAction obj) => NavigationController.PopToRootViewController(true)));
                        this.PresentViewController(timerEndAlert, true, null);
                        UILabel totalAnswersLabel = new UILabel();
                        UILabel rightAnswersLabel = new UILabel();
                        UILabel errorsLabel = new UILabel();

                        totalAnswersLabel.Text = Localizable.Str("Всего ответов: ") + $"{errors + rightAnswers}";
                        totalAnswersLabel.Font = UIFont.SystemFontOfSize(18);
                        NSMutableAttributedString rightAnswersString = new NSMutableAttributedString(Localizable.Str("Правильных ответов: "), font: UIFont.SystemFontOfSize(18));
                        rightAnswersString.Append(new NSAttributedString(rightAnswers.ToString(), font: UIFont.SystemFontOfSize(18), foregroundColor: UIColor.Green));
                        rightAnswersString.Append(new NSAttributedString("/", font: UIFont.SystemFontOfSize(18)));
                        rightAnswersString.Append(new NSAttributedString(NumberOfRightAnswers().ToString(), font: UIFont.SystemFontOfSize(18)));
                        rightAnswersLabel.AttributedText = rightAnswersString;

                        NSMutableAttributedString errorsString = new NSMutableAttributedString(Localizable.Str("Ошибок: "), font: UIFont.SystemFontOfSize(18));
                        errorsString.Append(new NSAttributedString(errors.ToString(), font: UIFont.SystemFontOfSize(18), foregroundColor: UIColor.Red));

                        errorsLabel.AttributedText = errorsString;

                        timerEndAlert.View.AddSubview(totalAnswersLabel);
                        timerEndAlert.View.AddSubview(rightAnswersLabel);
                        timerEndAlert.View.AddSubview(errorsLabel);


                        totalAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        totalAnswersLabel.TopAnchor.ConstraintEqualTo(timerEndAlert.View.TopAnchor, 55).Active = true;
                        totalAnswersLabel.LeftAnchor.ConstraintEqualTo(timerEndAlert.View.LeftAnchor, 15).Active = true;

                        rightAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        rightAnswersLabel.TopAnchor.ConstraintEqualTo(totalAnswersLabel.BottomAnchor, 5).Active = true;
                        rightAnswersLabel.LeftAnchor.ConstraintEqualTo(timerEndAlert.View.LeftAnchor, 15).Active = true;

                        errorsLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        errorsLabel.TopAnchor.ConstraintEqualTo(rightAnswersLabel.BottomAnchor, 5).Active = true;
                        errorsLabel.LeftAnchor.ConstraintEqualTo(timerEndAlert.View.LeftAnchor, 15).Active = true;
                        errorsLabel.BottomAnchor.ConstraintEqualTo(timerEndAlert.View.BottomAnchor, -55).Active = true;

                    }
                }
            });

            if (secondsForTimer != 0)
            {
                IsTimerOn = true;
                NSRunLoop.Main.AddTimer(timer, NSRunLoopMode.Common);
                timer.Fire();

            }
            var rightItem = new UIBarButtonItem(customView: timerLabel);
            NavigationItem.RightBarButtonItem = rightItem;



            base.ViewDidLoad();
            try
            {
                testDataOfCells = new AllCellsData(questionsList);

                for (int i = 0; i < questionsList.Count; i++)
                    colorsForQuestionsList.Add(null);

                QuestionListCollection = new UICollectionView(new CGRect(0, 0, 100, 100), new UICollectionViewLayout());
                NavigationItem.Title = Localizable.Str("Экзамен");

                View.AddSubview(QuestionListCollection);

                QuestionListCollection.TranslatesAutoresizingMaskIntoConstraints = false;

                QuestionListCollection.TopAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.TopAnchor, 5).Active = true;
                QuestionListCollection.LeftAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.LeftAnchor, 5).Active = true;
                QuestionListCollection.RightAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.RightAnchor, -5).Active = true;
                QuestionListCollection.HeightAnchor.ConstraintEqualTo(48).Active = true;


                QuestionListCollection.RegisterClassForCell(typeof(QuastionsListCell), QuastionsListCell.reuseIdentifier);

                ApplyButton = new UIButton(UIButtonType.System);
                ApplyButton.SetTitle(Localizable.Str("Ответить"), UIControlState.Normal);
                ApplyButton.Font = UIFont.SystemFontOfSize(16);


                ApplyButton.TouchUpInside += (o, e) =>
                {

                    int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;

                    //if (!testData.IsMultipleAnswersAvailable)
                    //{
                    //    int selectedAnswer = testDataOfCells[cellIndex].GetSelectedIndex();

                    //    if (selectedAnswer == -1)
                    //    {
                    //        UIView.Transition(AnswersCollectionView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { AnswersCollectionView.ReloadData(); }, null);

                    //    }
                    //    else if (questionsList[cellIndex].answers[selectedAnswer].IsCorrect)
                    //    {
                    //        colorsForQuestionsList[cellIndex] = green;
                    //        testDataOfCells[cellIndex].UserInteraction = false;
                    //        ApplyButton.Enabled = false;
                    //        testDataOfCells[cellIndex][selectedAnswer].backgroundColor = green;
                    //        rightAnswers++;
                    //        questionsAnswered++;
                    //    }
                    //    else
                    //    {
                    //        colorsForQuestionsList[cellIndex] = red;
                    //        testDataOfCells[cellIndex].UserInteraction = false;
                    //        ApplyButton.Enabled = false;
                    //        testDataOfCells[cellIndex][selectedAnswer].backgroundColor = red;
                    //        testDataOfCells[cellIndex][GetRightIndex(questionsList, cellIndex)].backgroundColor = yellow;
                    //        errors++;
                    //        questionsAnswered++;
                    //    }

                    //}
                    //else
                    //{
                    List<int> selectedAnswers = testDataOfCells[cellIndex].GetSelectedIndexes();

                    bool rightChecked = false;
                    bool wrondChecked = false;

                    if (selectedAnswers.Count == 0)
                    {
#if DEBUG

                        UIView.Transition(AnswersCollectionView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { AnswersCollectionView.ReloadData(); }, null);
#endif
                    }
                    else
                    {
                        questionsAnswered++;
                        for (int i = 0; i < testDataOfCells[cellIndex].answers.Count; i++)
                        {
                            if (testDataOfCells[cellIndex][i].IsChecked && questionsList[cellIndex].answers[i].IsCorrect)
                            {
                                rightAnswers++;
                                testDataOfCells[cellIndex][i].backgroundColor = green;
                                rightChecked = true;
                            }
                            else if (testDataOfCells[cellIndex][i].IsChecked && !questionsList[cellIndex].answers[i].IsCorrect)
                            {
                                errors++;
                                testDataOfCells[cellIndex][i].backgroundColor = red;
                                //wrondChecked = true;
                            }
                            else if (!testDataOfCells[cellIndex][i].IsChecked && questionsList[cellIndex].answers[i].IsCorrect)
                            {
                                wrondChecked = true;
                                testDataOfCells[cellIndex][i].backgroundColor = yellow;
                            }
                        }
                        testDataOfCells[cellIndex].UserInteraction = false;
                        ApplyButton.Enabled = false;
                        if (rightChecked && wrondChecked)
                        {
                            colorsForQuestionsList[cellIndex] = yellow;
                        }
                        else if (rightChecked && !wrondChecked)
                        {
                            colorsForQuestionsList[cellIndex] = green;
                        }
                        else
                        {
                            colorsForQuestionsList[cellIndex] = red;
                        }
                    }
                    //}


                    UIView.Transition(AnswersCollectionView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve,
                       () => { AnswersCollectionView.ReloadItems(AnswersCollectionView.IndexPathsForVisibleItems); }, null);
                    //UIView.Transition(AnswersCollectionView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { AnswersCollectionView.ReloadData(); }, null);
                    UIView.Transition(QuestionListCollection, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () => { QuestionListCollection.ReloadData(); }, null);

                    var cell = (AnswersCollectionCell)AnswersCollectionView.DequeueReusableCell("answersCollectionCell", NSIndexPath.FromItemSection(cellIndex, 0));
                    cell.answersTable.ReloadSections(NSIndexSet.FromIndex(0), UITableViewRowAnimation.Fade);
                    //                    testDataOfCells[cellIndex].answers[0].IsChecked = true;
                    //                    testDataOfCells[cellIndex].answers[1].IsChecked = false;
                    //                    UIView.Transition(AnswersCollectionView, 0.35, UIViewAnimationOptions.TransitionCrossDissolve,
                    //() => { AnswersCollectionView.ReloadItems(AnswersCollectionView.IndexPathsForVisibleItems); }, null);



                    if (questionsAnswered == questionsList.Count)
                    {
                        var endAlert = UIAlertController.Create(Localizable.Str("Тест окончен"), null, UIAlertControllerStyle.Alert);
                        endAlert.AddAction(UIAlertAction.Create(Localizable.Str("Ок"), UIAlertActionStyle.Cancel, (UIAlertAction obj) => NavigationController.PopToRootViewController(true)));
                        this.PresentViewController(endAlert, true, null);
                        UILabel totalAnswersLabel = new UILabel();
                        UILabel rightAnswersLabel = new UILabel();
                        UILabel errorsLabel = new UILabel();

                        totalAnswersLabel.Text = Localizable.Str("Ответов дано: ") + $"{errors + rightAnswers}";
                        totalAnswersLabel.Font = UIFont.SystemFontOfSize(18);
                        NSMutableAttributedString rightAnswersString = new NSMutableAttributedString(Localizable.Str("Правильных ответов: "), font: UIFont.SystemFontOfSize(18));
                        rightAnswersString.Append(new NSAttributedString(rightAnswers.ToString(), font: UIFont.SystemFontOfSize(18), foregroundColor: UIColor.Green));
                        rightAnswersString.Append(new NSAttributedString("/", font: UIFont.SystemFontOfSize(18)));
                        rightAnswersString.Append(new NSAttributedString(NumberOfRightAnswers().ToString(), font: UIFont.SystemFontOfSize(18)));
                        rightAnswersLabel.AttributedText = rightAnswersString;

                        NSMutableAttributedString errorsString = new NSMutableAttributedString(Localizable.Str("Ошибок: "), font: UIFont.SystemFontOfSize(18));
                        errorsString.Append(new NSAttributedString(errors.ToString(), font: UIFont.SystemFontOfSize(18), foregroundColor: UIColor.Red));

                        errorsLabel.AttributedText = errorsString;

                        endAlert.View.AddSubview(totalAnswersLabel);
                        endAlert.View.AddSubview(rightAnswersLabel);
                        endAlert.View.AddSubview(errorsLabel);


                        totalAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        totalAnswersLabel.TopAnchor.ConstraintEqualTo(endAlert.View.TopAnchor, 55).Active = true;
                        totalAnswersLabel.LeftAnchor.ConstraintEqualTo(endAlert.View.LeftAnchor, 15).Active = true;

                        rightAnswersLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        rightAnswersLabel.TopAnchor.ConstraintEqualTo(totalAnswersLabel.BottomAnchor, 5).Active = true;
                        rightAnswersLabel.LeftAnchor.ConstraintEqualTo(endAlert.View.LeftAnchor, 15).Active = true;

                        errorsLabel.TranslatesAutoresizingMaskIntoConstraints = false;
                        errorsLabel.TopAnchor.ConstraintEqualTo(rightAnswersLabel.BottomAnchor, 5).Active = true;
                        errorsLabel.LeftAnchor.ConstraintEqualTo(endAlert.View.LeftAnchor, 15).Active = true;
                        errorsLabel.BottomAnchor.ConstraintEqualTo(endAlert.View.BottomAnchor, -55).Active = true;



                    }

                    //UIView.Transition(cell.answersTable, 0.35, UIViewAnimationOptions.TransitionCrossDissolve, () =>
                    //{
                    //    cell.answersTable.BeginUpdates();
                    //    cell.answersTable.ReloadRows(new NSIndexPath[] { NSIndexPath.FromRowSection(2, 0) }, UITableViewRowAnimation.Fade);
                    //    cell.answersTable.EndUpdates();
                    //}, null);
                    //var cells = cell.answersTable.VisibleCells;
                };


                View.AddSubview(ApplyButton);
                ApplyButton.TranslatesAutoresizingMaskIntoConstraints = false;
                ApplyButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
                ApplyButton.CenterXAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.CenterXAnchor).Active = true;
                ApplyButton.WidthAnchor.ConstraintEqualTo(90).Active = true;
                ApplyButton.UserInteractionEnabled = true;
                ApplyButton.Enabled = true;

                NextButton = new UIButton(UIButtonType.System);
                PreviewButton = new UIButton(UIButtonType.System);

                NextButton.SetTitle(Localizable.Str("Далее"), UIControlState.Normal);
                PreviewButton.SetTitle(Localizable.Str("Назад"), UIControlState.Normal);

                View.AddSubview(NextButton);
                View.AddSubview(PreviewButton);

                NextButton.TranslatesAutoresizingMaskIntoConstraints = false;

                NextButton.LeftAnchor.ConstraintEqualTo(ApplyButton.RightAnchor, 30).Active = true;
                NextButton.RightAnchor.ConstraintEqualTo(View.RightAnchor, -30).Active = true;
                NextButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
                NextButton.HeightAnchor.ConstraintEqualTo(ApplyButton.HeightAnchor).Active = true;

                //NextButton.TitleEdgeInsets = new UIEdgeInsets(top: -12, left: -50, bottom: -12, right: -16);


                PreviewButton.TranslatesAutoresizingMaskIntoConstraints = false;

                PreviewButton.LeftAnchor.ConstraintEqualTo(View.LeftAnchor, 30).Active = true;
                PreviewButton.RightAnchor.ConstraintEqualTo(ApplyButton.LeftAnchor, -30).Active = true;
                PreviewButton.BottomAnchor.ConstraintEqualTo(View.SafeAreaLayoutGuide.BottomAnchor, -5).Active = true;
                PreviewButton.HeightAnchor.ConstraintEqualTo(ApplyButton.HeightAnchor).Active = true;

                NextButton.TouchUpInside +=
                (o, e) =>
                {
                    int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;
                    if (cellIndex != questionsList.Count - 1)
                        AnswersCollectionView.ScrollToItem(NSIndexPath.FromItemSection(cellIndex + 1, 0), UICollectionViewScrollPosition.CenteredHorizontally, false);
                };
                PreviewButton.TouchUpInside +=
                (o, e) =>
                {
                    int cellIndex = AnswersCollectionView.IndexPathsForVisibleItems[0].Row;
                    if (cellIndex != 0)
                        AnswersCollectionView.ScrollToItem(NSIndexPath.FromItemSection(cellIndex - 1, 0), UICollectionViewScrollPosition.CenteredHorizontally, false);
                };


                AnswersCollectionView = new UICollectionView(new CGRect(0, 0, 100, 100), new UICollectionViewLayout());
                View.AddSubview(AnswersCollectionView);
                AnswersCollectionView.TranslatesAutoresizingMaskIntoConstraints = false;

                AnswersCollectionView.TopAnchor.ConstraintEqualTo(QuestionListCollection.BottomAnchor, 10).Active = true;
                AnswersCollectionView.LeftAnchor.ConstraintEqualTo(View.LeftAnchor).Active = true;
                AnswersCollectionView.RightAnchor.ConstraintEqualTo(View.RightAnchor).Active = true;
                AnswersCollectionView.BottomAnchor.ConstraintEqualTo(ApplyButton.TopAnchor, -5).Active = true;

                AnswersCollectionView.RegisterClassForCell(typeof(AnswersCollectionCell), AnswersCollectionCell.reuseIdentifier);
                AnswersCollectionView.Source = new AnswersCollectionViewSource(testDataOfCells, questionsList, testData.IsMultipleAnswersAvailable);


                AnswersCollectionView.ReloadData();

                QuestionListCollection.Source = new QuestionListCollectionSource(questionsList.Count, AnswersCollectionView, colorsForQuestionsList, testDataOfCells);
                loopTimer = NSTimer.CreateRepeatingTimer(0.6, (t) =>
                {
                    int index = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1) == null ?
                        0 : AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row;
                    ApplyButton.Enabled = testDataOfCells[index].UserInteraction;
                    PreviewButton.Enabled = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row != 0;
                    NextButton.Enabled = AnswersCollectionView.IndexPathsForVisibleItems.ElementAtOrDefault(AnswersCollectionView.IndexPathsForVisibleItems.Length - 1).Row !=
                        questionsList.Count - 1;
                });
                NSRunLoop.Main.AddTimer(loopTimer, NSRunLoopMode.Common);

                QuestionListCollection.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;
                AnswersCollectionView.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;

                View.BackgroundColor = IsDark.Dark ? UIColor.Black : UIColor.White;
            }


            catch
            {
                var alert = UIAlertController.Create(Localizable.Str("Ошибка!"), null, UIAlertControllerStyle.Alert);
                var atr = new NSAttributedString(Localizable.Str("Ошибка при чтении") + $" {testData.FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                alert.SetValueForKey(atr, new NSString("attributedTitle"));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Ок :("), UIAlertActionStyle.Cancel, (UIAlertAction obj) => NavigationController.PopToRootViewController(true)));
                this.PresentViewController(alert, true, null);
            }

        }

        public override void ViewDidLayoutSubviews()
        {
            base.ViewDidLayoutSubviews();
            try
            {

                var cellSize2 = new CGSize(AnswersCollectionView.Bounds.Width, AnswersCollectionView.Bounds.Height);
                AnswersCollectionView.PagingEnabled = true;
                var layout2 = new UICollectionViewFlowLayout();
                layout2.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
                layout2.ItemSize = cellSize2;
                layout2.MinimumLineSpacing = 0;
                layout2.MinimumInteritemSpacing = 0;
                AnswersCollectionView.SetCollectionViewLayout(layout2, animated: true);
                AnswersCollectionView.ReloadData();



                var cellSize = new CGSize(QuestionListCollection.Bounds.Height - 5, QuestionListCollection.Bounds.Height - 5);
                //QuestionListCollection.PagingEnabled = true;
                var layout = new UICollectionViewFlowLayout();
                layout.ScrollDirection = UICollectionViewScrollDirection.Horizontal;
                layout.ItemSize = cellSize;
                QuestionListCollection.SetCollectionViewLayout(layout, animated: true);
            }
            catch
            {

                var alert = UIAlertController.Create(Localizable.Str("Ошибка!"), null, UIAlertControllerStyle.Alert);
                var atr = new NSAttributedString(Localizable.Str("Ошибка при чтении") + $" {testData.FileName}", foregroundColor: UIColor.Red, font: UIFont.SystemFontOfSize(20));
                alert.SetValueForKey(atr, new NSString("attributedTitle"));
                alert.AddAction(UIAlertAction.Create(Localizable.Str("Ок :("), UIAlertActionStyle.Cancel, (UIAlertAction obj) => NavigationController.PopToRootViewController(true)));
                this.PresentViewController(alert, true, null);
            }
        }
        int NumberOfRightAnswers()
        {
            int result = 0;
            for (int i = 0; i < questionsList.Count; i++)
            {
                result += questionsList[i].NumberOfRightAnswers;
                //for (int j = 0; j < questionsList[i].Count; j++)
                //{
                //    if (questionsList[i][j].IsCorrect)
                //        result++;
                //}

            }
            return result;

        }

        int GetRightIndex(List<Questions> Questionss, int AtIndex)
        {
            for (int i = 0; i < Questionss[AtIndex].answers.Count; i++)
            {
                if (Questionss[AtIndex].answers[i].IsCorrect)
                    return i /*- 1*/;

            }
            return -1; ///
        }

        public override void ViewDidDisappear(bool animated)
        {
            timer.Invalidate();
            timer.Dispose();
            timer = null;
            var source = (AnswersCollectionViewSource)AnswersCollectionView.Source;
            loopTimer.Invalidate();
            loopTimer.Dispose();
            loopTimer = null;
        }
    }
}
